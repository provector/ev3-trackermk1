package provector.trackerMk1.ev3;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;

import lejos.hardware.motor.EV3LargeRegulatedMotor;
import lejos.hardware.motor.EV3MediumRegulatedMotor;
import lejos.hardware.port.MotorPort;
import lejos.hardware.port.Port;
import lejos.hardware.port.SensorPort;
import lejos.utility.Delay;

public class TrackerMk1 {
	
	/*
	 * TODO: [ ] MotorACC in % from config
	 * TODO: [ ] MotorSpeed in % from config
	 * TODO: [ ] Add CPU metrics
	 * TODO: [ ] Add PowerSaving Mode (Sensors not constantly on)
	 * TODO: [?] Make default config class
	 */
	
	/*
	 * constants
	 */
	
	public final static String VERSION = "0.9.01 Alpha (Completely untested).";
	
	private final static int COMMAND_PORT = 19231;
	protected final static int VIDEO_SERVICE_PORT = 19232;
	
	protected final static long TELEMETRY_POLL_TIME_MS = 1000;
	
	protected final static int DEFAULT_VIDEO_WIDTH = 320;
	protected final static int DEFUALT_VIDEO_HEIGHT = 240;
	
	protected final static Port ULTRASONIC_SENSOR_PORT = SensorPort.S2;
	private final static Port LEFT_MOTOR_PORT = MotorPort.D;
	private final static Port RIGHT_MOTOR_PORT = MotorPort.A;
	private final static Port CAMERA_MOTOR_PORT = MotorPort.C;
	
	public final static int MAX_LARGE_MOTOR_SPEED = 960; //opt 960-1020 deg/s
	public final static int MAX_MEDIUM_MOTOR_SPEED = 1440; //opt 1440-1500 deg/s
	public final static int MAX_ACC_VALUE = 6000; //100%
	
	private final static String LOGSTR = "Main: ";
		
	/*
	 * static variables
	 */
	
	private static ServerSocket _commandServer;
	private static Socket _commandSocket;
	private static BufferedReader _commandReader;
	private static VideoStreamer _videoService;
	private static Thread _videoServiceThread;
	private static TelemetryService _telemetryService;
	private static Thread _telemetryServiceThread;
	private static String _clientIP;
	
	private static EV3LargeRegulatedMotor _leftMotor;
	private static EV3LargeRegulatedMotor _rightMotor;
	private static EV3MediumRegulatedMotor _cameraMotor; 
	
	private static int _mainMotorSpeed;
	private static int _mainMotorAcc;
	private static int _cameraMotorSpeed;
	private static int _cameraMotorAcc;
	
	/*
	 * loop conditions
	 */
	
	private static boolean _mainLoop = true;
	private static boolean RECEIVE_COMMANDS = true;
	
	

	public static void main(String[] args) {
		
		log("[INFO] Starting TrackerMk1 Firmware ver "+VERSION);
		
		startServer();
		
		while(_mainLoop) {
			
			initStuff();
			
			createSocket(); //blocking call
		
			startVideoService();
			
			startTelemetryService();
			
			processCommands(); //blocking call (while loop)
			
			//do housekeeping
			clearVariables();
			stopServices();
			closeClientConnection();
			scheduleGC(1000);			
		
		}//endMAINLOOP

		closeServer();
		log("[INFO] Closing main Program");
		
	}//endMain
	
	

	private static void processCommands() {
		String currentLine = null;
		
		//TODO: create separate method
		while(RECEIVE_COMMANDS) {
			try {
				currentLine=_commandReader.readLine();
			}catch(IOException io) {
				log("[ERROR] IO Error on command pipeline: "+io.getMessage());
				RECEIVE_COMMANDS = false;
				break;
			}
			//process multi commands
			if(currentLine.startsWith("CONFIG")) {
				String[] configArr = currentLine.split(":");
				if(configArr.length==3) {
					String key = configArr[1];
					String value = configArr[2];
					
					switch(key) {
					case	"MM_SPEED"	:
										setMainMotorSpeed(value);
										break;
					case	"MM_ACC"	:
										setMainMotorAcc(value);
										break;
					case 	"CM_SPEED"	:
										setCameraMotorSpeed(value);
										break;
					case	"CM_ACC"	:
										setCameraMotorAcc(value);
										break;
					default:
						log("[WARN] Unrecognized config key: "+key);
					}//endSWITCH
					
				}else{
					log("[WARN] Inconsistent config data ("+currentLine+"), skipping...");
					continue;
				}//endIF
				
			}else{
				
				//process single commands
				switch(currentLine) {
				case "FORWARD_GO"		:
										motorsForward();
										break;										
				case "FORWARD_STOP"		:	
										motorsStop();
										break;
				case "BACKWARD_GO"		:		
										motorsBackwards();
										break;
				case "BACKWARD_STOP"	:
										motorsStop();
										break;
				case "LEFT_TURN_GO"		:
										motorsLeft();
										break;
				case "LEFT_TURN_STOP"	:	
										motorsStop();
										break;
				case "RIGHT_TURN_GO"	:	
										motorsRight();
										break;
				case "RIGHT_TURN_STOP"	:
										motorsStop();
										break;
				case "CAMERA_LEFT_GO"	:	
										cameraMotorLeft();
										break;
				case "CAMERA_LEFT_STOP"	:
										cameraMotorStop();
										break;
				case "CAMERA_RIGHT_GO"	:
										cameraMotorRight();
										break;
				case "CAMERA_RIGHT_STOP":
										cameraMotorStop();
										break;
				case "DISCONNECT"		:
										triggerDisconnect();
										break;
				case "SHUTDOWN"			:
										triggerShutdown();
										break;
				default:
					log("[WARN] Unrecognized command!: "+currentLine);
				}//endSWTICH
			}//endIFStartsWith
								
		}//whileRECEIVE_COMMANDS
	}
	
	private static void scheduleGC(long delay) {
		Runtime.getRuntime().gc();
		Delay.msDelay(delay);
	}
	
	private static void triggerDisconnect() {
		log("[INFO] Triggering disconnect.");
		RECEIVE_COMMANDS=false;
	}
	
	private static void triggerShutdown() {
		log("[INFO] Triggering shutdown.");
		_mainLoop = false;
		RECEIVE_COMMANDS=false;
	}
	
	private static void clearVariables() {
		_mainMotorSpeed = 0;
		_mainMotorAcc = 0;
		_cameraMotorSpeed = 0;
		_cameraMotorAcc = 0;
		
		_leftMotor = null;
		_rightMotor = null;
		_cameraMotor = null;
		log("[DEBUG] Variables nulled");
		
	}
	
	private static void startServer() {
		try {
			_commandServer = new ServerSocket(COMMAND_PORT);
		} catch (IOException e) {
			log("[FATAL] Error creating server: "+e.getMessage());
		}
		log("[INFO] Server created on port "+COMMAND_PORT);
	}
	
	private static void closeServer() {
		log("[INFO] Closing command server...");
		try {
			_commandServer.close();
		
		}catch(IOException io) {
			log("[ERROR] While closing server: "+io.getMessage());
		}
		_commandServer = null;
		log("[INFO] Command server closed");
	}
			
	private static void closeClientConnection() {
		log("[INFO] Closing client connection...");
		try {
			_commandReader.close();
			_commandSocket.close();
		}catch(IOException io) {
			log("[ERROR] when closing client connection: "+io.getMessage());
		}
		_commandReader = null;
		_commandSocket = null;
		_clientIP = null;
		log("[INFO] Client connection closed.");
	}
	
	private static void stopServices() {
		log("[INFO] Stopping services... Video");
		if(_videoService!=null&&_videoServiceThread.isAlive()) {
			while(_videoServiceThread.isAlive()) {
				Delay.msDelay(100);
			}
		}else {
			log("[WARN] Video service already stopped.");
		}
		_videoServiceThread = null;
		_videoService = null;
		log("[INFO] Video done.");
		log("[INFO] Stopping services... Telemetry");
		if(_telemetryService!=null&&_telemetryServiceThread.isAlive()) {
			while(_telemetryServiceThread.isAlive()) {
				Delay.msDelay(100);
			}
		}else {
			log("[WARN] Telemetry service already stopped.");
		}
		_telemetryServiceThread = null;
		_telemetryService = null;
		log("[INFO] Telemetry done.");
		log("[INFO] Services stopped.");
	}
	
	private static void startTelemetryService() {
		_telemetryService = new TelemetryService(_commandSocket);
		_telemetryServiceThread = new Thread(_telemetryService);
		_telemetryServiceThread.start();
		//TODO:join? telemetryServiceThread.join();
	}
	
	private static void startVideoService() {
		_videoService = new VideoStreamer(_clientIP);
		_videoServiceThread = new Thread(_videoService);
		_videoServiceThread.start();
		//TODO:join? videoServiceThread.join();
	}
	
	private static void createSocket() {
		try {
			_commandSocket = _commandServer.accept(); //blocking call
			_clientIP = _commandSocket.getRemoteSocketAddress().toString();
			_commandReader = new BufferedReader(new InputStreamReader(_commandSocket.getInputStream()));
		} catch (IOException e) {
			log("[ERROR ] While starting command server: "+e.getMessage());
		}
		log("[INFO] Command Server started.");
	}
		
	private static void initStuff() {
		log("[INFO] Initiating motors...");
		_leftMotor = new EV3LargeRegulatedMotor(LEFT_MOTOR_PORT);
		_rightMotor = new EV3LargeRegulatedMotor(RIGHT_MOTOR_PORT);
		_cameraMotor = new EV3MediumRegulatedMotor(CAMERA_MOTOR_PORT);	
	}
	
	private static void setMainMotorSpeed(String configValue) {
		try {
			int speed = Integer.parseInt(configValue);
			setMainMotorSpeed(speed);
		}catch(NumberFormatException n) {
			log("[ERROR] Incorrect value: "+configValue);
			return;
		}
	}
	
	private static void setMainMotorSpeed(int speed) {
		speed=Math.abs(speed);
		if(speed>MAX_LARGE_MOTOR_SPEED) {
			_mainMotorSpeed = MAX_LARGE_MOTOR_SPEED;
			log("[WARN] Large motor speed value "+speed+" over maximum threshold, setting max");
		}else {
			_mainMotorSpeed = speed;
		}
		_leftMotor.setSpeed(_mainMotorSpeed);
		_rightMotor.setSpeed(_mainMotorSpeed);
	}
	
	private static void setMainMotorAcc(String configValue) {
		try {
			int acc = Integer.parseInt(configValue);
			setMainMotorAcc(acc);
		}catch(NumberFormatException n) {
			log("[ERROR] Incorrect value: "+configValue);
			return;
		}
	}
	
	private static void setMainMotorAcc(int accValue) {
		accValue=Math.abs(accValue);
		if(accValue>MAX_ACC_VALUE) {
			_mainMotorAcc = MAX_ACC_VALUE;
			log("[WARN] Large motor acceleration value "+accValue+" over maximum threshold, setting max");
		}else {
			_mainMotorAcc = accValue;
		}
		_leftMotor.setAcceleration(_mainMotorAcc);
		_rightMotor.setAcceleration(_mainMotorAcc);
	}
	
	private static void setCameraMotorSpeed(String configValue) {
		try {
			int speed = Integer.parseInt(configValue);
			setCameraMotorSpeed(speed);
		}catch(NumberFormatException n) {
			log("[ERROR] Incorrect value: "+configValue);
			return;
		}
	}
	
	private static void setCameraMotorSpeed(int speed) {
		speed=Math.abs(speed);
		if(speed>MAX_MEDIUM_MOTOR_SPEED) {
			_cameraMotorSpeed = MAX_MEDIUM_MOTOR_SPEED;
			log("[WARN] Medium motor speed value "+speed+" over maximum threshold, setting max");
		}else {
			_cameraMotorSpeed = speed;
		}
		_cameraMotor.setSpeed(_cameraMotorSpeed);
	}
	
	private static void setCameraMotorAcc(String configValue) {
		try {
			int acc = Integer.parseInt(configValue);
			setCameraMotorAcc(acc);
		}catch(NumberFormatException n) {
			log("[ERROR] Incorrect value: "+configValue);
			return;
		}
	}
	
	private static void setCameraMotorAcc(int accValue) {
		accValue=Math.abs(accValue);
		if(accValue>MAX_ACC_VALUE) {
			_cameraMotorAcc = MAX_ACC_VALUE;
			log("[WARN] Medium motor acceleration value "+accValue+" over maximum threshold, setting max");
		}else {
			_cameraMotorAcc = accValue;
		}
		_cameraMotor.setAcceleration(_cameraMotorAcc);
	}
	
	private static void motorsStop() {
		_leftMotor.stop();
		_rightMotor.stop();
	}
	
	private static void motorsForward() {
		_leftMotor.backward();
		_rightMotor.backward();
	}
	
	private static void motorsBackwards() {
		_leftMotor.forward();
		_rightMotor.forward();
	}
	
	private static void motorsLeft() {
		_rightMotor.backward();
		_leftMotor.forward();
	}
	
	private static void motorsRight() {
		_rightMotor.forward();
		_leftMotor.backward();
	}
	
	private static void cameraMotorLeft() {
		_cameraMotor.forward();
	}
	
	private static void cameraMotorRight() {
		_cameraMotor.backward();
	}
	
	private static void cameraMotorStop() {
		_cameraMotor.stop();
	}
	
	private static void log(String s) {
		System.out.println(LOGSTR+s);
	}
	
}
