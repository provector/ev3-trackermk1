package provector.trackerMk1.ev3;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;

import lejos.hardware.ev3.EV3;
import lejos.hardware.motor.EV3LargeRegulatedMotor;
import lejos.hardware.motor.EV3MediumRegulatedMotor;
import lejos.hardware.port.MotorPort;
import lejos.hardware.port.Port;
import lejos.hardware.port.SensorPort;
import lejos.hardware.sensor.EV3UltrasonicSensor;
import lejos.utility.Delay;
import provector.trackerMk1.shared.entities.Command;

public class TrackerMk1 {
	
	private final static int COMMAND_PORT = 19231;
	
	protected final static Port ULTRASONIC_SENSOR_PORT = SensorPort.S2;
	private final static Port LEFT_MOTOR_PORT = MotorPort.D;
	private final static Port RIGHT_MOTOR_PORT = MotorPort.A;
	private final static Port CAMERA_MOTOR_PORT = MotorPort.C;
	
	public final static int MAX_LARGE_MOTOR_SPEED = 960; //opt 960-1020 deg/s
	public final static int MAX_MEDIUM_MOTOR_SPEED = 1440; //opt 1440-1500 deg/s
	public final static int MAX_ACC_VALUE = 6000; //100%
	
	private static int _largeMotorSpeed;
	private static int _mediumMotorSpeed;
		
	private static ServerSocket _commandServer;
	private static Socket _commandSocket;
	private static BufferedReader _commandReader;
	private static VideoStreamer _videoService;
	private static Thread _videoServiceThread;
	private static TelemetryService _telemetryService;
	private static Thread _telemetryServiceThread;
	private static String _clientIP;
	
	private static int _mainMotorSpeed;
	private static int _mainMotorAcc;
	private static int _cameraMotorSpeed;
	private static int _cameraMotorAcc;
	
	private static EV3LargeRegulatedMotor _leftMotor;
	private static EV3LargeRegulatedMotor _rightMotor;
	private static EV3MediumRegulatedMotor _cameraMotor; 
		
	private final static String LOGSTR = "Main: ";
	
	private static boolean _mainLoop = true;
	
	private static boolean RECEIVE_COMMANDS = true;
	
	public static void main(String[] args) {
		
		while(_mainLoop) {
			
			initStuff();
			
			startCommandServer(); //blocking call
		
			startVideoService();
			
			startTelemetryService();
			
			String currentLine = null;
			
			while(RECEIVE_COMMANDS) {
				try {
					currentLine=_commandReader.readLine();
				}catch(IOException io) {
					log("[ERROR] IO Error on command pipeline: "+io.getMessage());
					RECEIVE_COMMANDS = false;
					break;
				}
			
				switch(currentLine) {
				case "LM_SPEED"			:			
										break;
				case "FORWARD_GO"		:
										motorsForward();
										break;										
				case "FORWARD_STOP"		:	
										motorsStop();
										break;
				case "BACKWARD_GO"		:		
										motorsBackwards();
										break;
				case "BACKWARD_STOP"	:
										motorsStop();
										break;
				case "LEFT_TURN_GO"		:
										motorsLeft();
										break;
				case "LEFT_TURN_STOP"	:	
										motorsStop();
										break;
				case "RIGHT_TURN_GO"	:	
										motorsRight();
										break;
				case "RIGHT_TURN_STOP"	:
										motorsStop();
										break;
				case "CAMERA_LEFT_GO"	:	
										cameraMotorLeft();
										break;
				case "CAMERA_LEFT_STOP"	:
										cameraMotorStop();
										break;
				case "CAMERA_RIGHT_GO"	:
										cameraMotorRight();
										break;
				case "CAMERA_RIGHT_STOP":
										cameraMotorStop();
										break;
				case "DISCONNECT"		:
										triggerDisconnect();
										break;
				case "SHUTDOWN"			:
										triggerShutdown();
										break;
				default:
					log("[WARN] Unrecognized command!: "+currentLine);
				}//endSWTICH
				
			}//whileRECEIVE_COMMANDS
			
			clearVariables();
			//do housekeeping
			stopServices();
			closeClientConnection();
			//try gc
			Runtime.getRuntime().gc();
			Delay.msDelay(1000);
		
		}//endMAINLOOP

		closeServer();
		//exit program
		log("[INFO] Closing main Program");
		
	}//endMain
	
	private static void triggerDisconnect() {
		log("[INFO] Triggering disconnect.");
		RECEIVE_COMMANDS=false;
	}
	
	private static void triggerShutdown() {
		log("[INFO] Triggering shutdown.");
		_mainLoop = false;
		RECEIVE_COMMANDS=false;
	}
	
	private static void clearVariables() {
		_mainMotorSpeed = 0;
		_mainMotorAcc = 0;
		_cameraMotorSpeed = 0;
		_cameraMotorAcc = 0;
		
		_leftMotor = null;
		_rightMotor = null;
		_cameraMotor = null;
		log("[DEBUG] Variables nulled");
		
	}
	
	private static void closeServer() {
		log("[INFO] Closing command server...");
		try {
			_commandServer.close();
		
		}catch(IOException io) {
			log("[ERROR] While closing server: "+io.getMessage());
		}
		_commandServer = null;
		log("[INFO] Command server closed");
	}
		
	
	private static void closeClientConnection() {
		log("[INFO] Closing client connection...");
		try {
			_commandReader.close();
			_commandSocket.close();
		}catch(IOException io) {
			log("[ERROR] when closing client connection: "+io.getMessage());
		}
		_commandReader = null;
		_commandSocket = null;
		_clientIP = null;
		log("[INFO] Client connection closed.");
	}
	
	private static void stopServices() {
		log("[INFO] Stopping services... Video");
		if(_videoService!=null&&_videoServiceThread.isAlive()) {
			while(_videoServiceThread.isAlive()) {
				Delay.msDelay(100);
			}
		}else {
			log("[WARN] Video service already stopped.");
		}
		_videoServiceThread = null;
		_videoService = null;
		log("[INFO] Video done.");
		log("[INFO] Stopping services... Telemetry");
		if(_telemetryService!=null&&_telemetryServiceThread.isAlive()) {
			while(_telemetryServiceThread.isAlive()) {
				Delay.msDelay(100);
			}
		}else {
			log("[WARN] Telemetry service already stopped.");
		}
		_telemetryServiceThread = null;
		_telemetryService = null;
		log("[INFO] Telemetry done.");
		log("[INFO] Services stopped.");
	}
	
	private static void startTelemetryService() {
		_telemetryService = new TelemetryService(_commandSocket);
		_telemetryServiceThread = new Thread(_telemetryService);
		_telemetryServiceThread.start();
		//TODO:join? telemetryServiceThread.join();
	}
	
	private static void startVideoService() {
		_videoService = new VideoStreamer(_clientIP);
		_videoServiceThread = new Thread(_videoService);
		_videoServiceThread.start();
		//TODO:join? videoServiceThread.join();
	}
	
	private static void startCommandServer() {
		try {
			_commandServer = new ServerSocket(COMMAND_PORT);
			_commandSocket = _commandServer.accept(); //blocking call
			_clientIP = _commandSocket.getRemoteSocketAddress().toString();
			_commandReader = new BufferedReader(new InputStreamReader(_commandSocket.getInputStream()));
		} catch (IOException e) {
			log("Error starting command server: "+e.getMessage());
		}
		log("Command Server started.");
	}
		
	private static void initStuff() {
		log("Initiating motors...");
		_leftMotor = new EV3LargeRegulatedMotor(LEFT_MOTOR_PORT);
		_rightMotor = new EV3LargeRegulatedMotor(RIGHT_MOTOR_PORT);
		_cameraMotor = new EV3MediumRegulatedMotor(CAMERA_MOTOR_PORT);
		log("Initializing sensors...");
		
	}
	
	private void setMainMotorSpeed(int s) {
		s=Math.abs(s);
		if(s>MAX_LARGE_MOTOR_SPEED) {
			_mainMotorSpeed = MAX_LARGE_MOTOR_SPEED;
			log("[WARN] Large motor speed value "+s+" over maximum threshold, setting max");
		}else {
			_mainMotorSpeed = s;
		}
	}
	
	//TODO: make acc in %?
	
	private void setMainMotorAcc(int a) {
		a=Math.abs(a);
		if(a>MAX_ACC_VALUE) {
			_mainMotorAcc = MAX_ACC_VALUE;
			log("[WARN] Large motor acceleration value "+a+" over maximum threshold, setting max");
		}else {
			_mainMotorAcc = a;
		}
	}
	
	private void setCameraMotorSpeed(int s) {
		s=Math.abs(s);
		if(s>MAX_MEDIUM_MOTOR_SPEED) {
			_cameraMotorSpeed = MAX_MEDIUM_MOTOR_SPEED;
			log("[WARN] Medium motor speed value "+s+" over maximum threshold, setting max");
		}else {
			_cameraMotorSpeed = s;
		}
	}
	
	private void setCameraMotorAcc(int a) {
		a=Math.abs(a);
		if(a>MAX_ACC_VALUE) {
			_cameraMotorAcc = MAX_ACC_VALUE;
			log("[WARN] Medium motor acceleration value "+a+" over maximum threshold, setting max");
		}else {
			_cameraMotorAcc = a;
		}
	}
	
	private static void motorsStop() {
		_leftMotor.stop();
		_rightMotor.stop();
	}
	
	private static void motorsForward() {
		_leftMotor.backward();
		_rightMotor.backward();
	}
	
	private static void motorsBackwards() {
		_leftMotor.forward();
		_rightMotor.forward();
	}
	
	private static void motorsLeft() {
		_rightMotor.backward();
		_leftMotor.forward();
	}
	
	private static void motorsRight() {
		_rightMotor.forward();
		_leftMotor.backward();
	}
	
	private static void cameraMotorLeft() {
		_cameraMotor.forward();
	}
	
	private static void cameraMotorRight() {
		_cameraMotor.backward();
	}
	
	private static void cameraMotorStop() {
		_cameraMotor.stop();
	}
	
	private static void disconnectClient() {
		//TODO: implement
	}
	
	private static void log(String s) {
		System.out.println(LOGSTR+s);
	}
	
}
