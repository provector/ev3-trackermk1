package provector.trackerMk1.ev3;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.net.Socket;

import com.google.gson.Gson;

import lejos.hardware.Battery;
import lejos.hardware.sensor.EV3UltrasonicSensor;
import lejos.robotics.SampleProvider;
import lejos.utility.Delay;

public class TelemetryService implements Runnable {

	private EV3UltrasonicSensor ultrasonicSensor;
	
	private Gson GSON;
	
	private float batteryCurrent;
	private float motorCurrent;
	private float batteryVoltage;
	
	private long freeMemoryBytes;
	private long totalMemoryBytes;
	private long usedMemoryBytes;
	
	private int distanceSample;
	
	private SampleProvider currentDistanceSample;
	private Socket commandSocket;
	private Battery battery;
	private Runtime runtime;
	
	private BufferedWriter dataSender;
	
	private final String LOGSTR = "TSERVICE: ";
	private final long POLL_TIME = 1000; //ms 
	
	private boolean STOP = false;
	
	public TelemetryService(Socket commandSocket) {
		if(commandSocket==null) {
			log("FATAL: Command Socket is null!");
			return;
		}
		//get output pipeline
		try {
			dataSender = new BufferedWriter(new OutputStreamWriter(commandSocket.getOutputStream()));
		}catch(IOException io) {
			log("ERROR openning output stream: "+io.getMessage());
		}
		//get Runtime
		runtime = Runtime.getRuntime();
		//get Sensors
		ultrasonicSensor = new EV3UltrasonicSensor(TrackerMk1.ULTRASONIC_SENSOR_PORT);
		if(!ultrasonicSensor.isEnabled()) {
			ultrasonicSensor.enable();
		}else {
			log("WARN: Ultrasonic sensor was already enabled!");
		}
		battery = new Battery();
		GSON = new Gson();
		log("Construction finished.");
	}
	
	@Override
	public void run() {
		log("Starting telemetry service...");
		while(!STOP) {
			pollValues();
			sendValues();
			Delay.msDelay(POLL_TIME);
		}
	}
	
	private void pollValues() {
		this.batteryCurrent = battery.getBatteryCurrent();
		this.motorCurrent = battery.getMotorCurrent();
		this.batteryVoltage = battery.getVoltage();
		
		this.freeMemoryBytes = runtime.freeMemory();
		this.totalMemoryBytes = runtime.totalMemory();
		this.usedMemoryBytes = totalMemoryBytes - freeMemoryBytes;
		
		this.distanceSample = getDistanceCm();
		
		
	}
	
	private void sendValues() {
		
	}
	
	private int getDistanceCm() {
		
		currentDistanceSample = ultrasonicSensor.getDistanceMode();
		float[] dSample = new float[currentDistanceSample.sampleSize()];
		currentDistanceSample.fetchSample(dSample, 0);
		int distanceCm = (int) (dSample[0]*100);
		if(distanceCm>250) distanceCm = -1;
		return distanceCm;
	}
	
	public void stop() {
		this.STOP = true;
		log("Stop flag set to true");
	}
	
	private void log(String s) {
		System.out.println(LOGSTR+s);
	}

}
