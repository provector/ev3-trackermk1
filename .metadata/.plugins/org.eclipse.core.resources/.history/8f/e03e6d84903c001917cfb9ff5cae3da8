package provector.trackerMk1.ev3;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.net.Socket;

import com.google.gson.Gson;

import lejos.hardware.Battery;
import lejos.hardware.sensor.EV3UltrasonicSensor;
import lejos.robotics.SampleProvider;
import lejos.utility.Delay;
import provector.trackerMk1.shared.entities.TelemetryPacket;

public class TelemetryService implements Runnable {

	private EV3UltrasonicSensor ultrasonicSensor;
	
	private Gson GSON;
	
	private SampleProvider currentDistanceSample;
	private Runtime runtime;
	
	private BufferedWriter dataSender;
	private TelemetryPacket currentDataPacket;
	
	private final String LOGSTR = "TelemetryService: ";
	private final long POLL_TIME = TrackerMk1.TELEMETRY_POLL_TIME_MS; 
	
	private boolean STOP = false;
	private String hostIP;
	
	public TelemetryService(String ip) {
		
		this.hostIP = ip;
		//get Runtime
		runtime = Runtime.getRuntime();
		//get Sensors
		ultrasonicSensor = new EV3UltrasonicSensor(TrackerMk1.ULTRASONIC_SENSOR_PORT);
		if(!ultrasonicSensor.isEnabled()) {
			ultrasonicSensor.enable();
		}else {
			log("[WARN]: Ultrasonic sensor was already enabled!");
		}
		GSON = new Gson();
		log("[DEBUG] Construction finished.");
	}
	
	@Override
	public void run() {
		log("[INFO] Starting telemetry service...");
		try {
			Socket commandSocket = new Socket(hostIp);
			dataSender = new BufferedWriter(new OutputStreamWriter(commandSocket.getOutputStream()));
		}catch(IOException io) {
			log("[ERROR] openning output stream: "+io.getMessage());
		}
		while(!STOP) {
			pollValues();
			sendValues();
			Delay.msDelay(POLL_TIME);
		}
		cleanUp();
		log("[INFO] Telemetry Service thread finished.");
	}
	
	private void cleanUp() {
		try {
			this.dataSender.close();
		} catch (IOException e) {
			log("[WARN] Problems closing dataSender stream: "+e.getMessage());
		}
		if(ultrasonicSensor.isEnabled()) {
			ultrasonicSensor.disable();
		}else {
			log("[WARN] ultrasonic sensor was already disabled! ");
		}
		GSON = null;
	}
	
	private void pollValues() {
		
		currentDataPacket = new TelemetryPacket();
		currentDataPacket.setBatteryCurrent(Battery.getBatteryCurrent());
		currentDataPacket.setMotorCurrent(Battery.getMotorCurrent());
		currentDataPacket.setBatteryVoltage(Battery.getVoltage());
		
		currentDataPacket.setFreeMemoryBytes(runtime.freeMemory());
		currentDataPacket.setTotalMemoryBytes(runtime.totalMemory());
		
		currentDataPacket.setDistanceSample(getDistanceCm());
	}
	
	private void sendValues() {
		String dataString = GSON.toJson(currentDataPacket,TelemetryPacket.class);
		try {
			dataSender.write(dataString);
			log("[DEBUG] Data sent: "+dataString);
		} catch (IOException e) {
			log("ERROR while sending telemetry data, aborting: "+e.getMessage());
			this.STOP = true;
		}
	}
	
	private int getDistanceCm() {
		
		currentDistanceSample = ultrasonicSensor.getDistanceMode();
		float[] dSample = new float[currentDistanceSample.sampleSize()];
		currentDistanceSample.fetchSample(dSample, 0);
		int distanceCm = (int) (dSample[0]*100);
		if(distanceCm>250) distanceCm = -1;
		return distanceCm;
	}
	
	public void stop() {
		this.STOP = true;
		log("Stop flag set to true");
	}
	
	private void log(String s) {
		System.out.println(LOGSTR+s);
	}

}
