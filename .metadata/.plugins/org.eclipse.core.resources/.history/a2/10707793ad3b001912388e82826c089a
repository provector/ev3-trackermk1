package provector.trackerMk1.ev3;

import java.io.BufferedOutputStream;
import java.io.IOException;
import java.net.Socket;

import lejos.hardware.BrickFinder;
import lejos.hardware.ev3.EV3;
import lejos.hardware.lcd.LCD;
import lejos.hardware.video.Video;

public class VideoStreamer implements Runnable {

	private final int DEFAULT_WIDTH = 160;
	private final int DEFAULT_HEIGHT = 120;
	private final int DEFAULT_PORT = 19232;
	
	private final String LOGSTR = "VideoStreamer: ";
	
	private int _width;
	private int _height;
	
	private Video videoSource;
	private String hostIP;
	
	private boolean STOP = false;
	
	EV3 ev3;
	
	public VideoStreamer(String hostIP) {
		init(hostIP,DEFAULT_WIDTH,DEFAULT_HEIGHT);
	}
	
	public VideoStreamer(String hostIP,int width,int height) {
		init(hostIP,width,height);
	}
	
	private void init(String hostIP,int width,int height) {
		this.ev3 = (EV3) BrickFinder.getLocal();
		videoSource = ev3.getVideo();
		this.hostIP = hostIP;
		this._width = width;
		this._height = height;
	}
	
	@Override
	public void run() {
		log("[INFO] Starting videoStreaming thread...");
		Socket videoSocket;
		BufferedOutputStream  videoStream;
		try {
			videoSource.open(_width, _height);
			videoSocket = new Socket(this.hostIP,DEFAULT_PORT);
			videoStream = new BufferedOutputStream(videoSocket.getOutputStream());
		}catch(IOException io) {
			log("[ERROR] while openning video stream: "+io.getMessage());
			return;
		}
		//double check
		if(videoSocket==null||videoStream==null) {
			log("[FATAL]: videoSocket or videoStream is null!");
		}
		byte[] frame = videoSource.createFrame();
		long start = System.currentTimeMillis();
		int frameCounter = 0;
		LCD.drawString("Fps: ",0,0);
		log("[INFO] Thread Started.");
		while(!STOP) {
			try {
				videoSource.grabFrame(frame);
				//fps counter
				int fps = (int) (++frameCounter * 1000f/(System.currentTimeMillis()-start));
				videoStream.write(frame);
				//TODO: flush? videoStream.flush();
			}catch(IOException io) {
				}
		}//endWHILE
		
		//cleanup
		try {
			videoStream.close();
			videoSocket.close();
			videoSource.close();
		}catch(IOException io) {
			log("[ERROR] while cleaning up: "+io.getMessage());
		}
		hostIP = null;
		log("[INFO] Thread ended.");
	}
	
	protected void stop() {
		this.STOP = true;
		log("[INFO] Stop flag set to true");
	}
	
	private void log(String s) {
		System.out.println(LOGSTR+s);
	}
}
