package provector.trackerMk1.pc;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.geom.AffineTransform;
import java.awt.image.AffineTransformOp;
import java.awt.image.BufferedImage;
import java.io.BufferedInputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

import javax.swing.JFrame;
import javax.swing.JInternalFrame;
import javax.swing.JPanel;

import tests.examples.Ev3VideoStreamTestApp_PC;

public class VideoService implements Runnable {

	public static final int PORT = 19232;

	private static final int OUTPUT_WIDTH = 640;
	private static final int OUTPUT_HEIGHT = 320;

	private static final int ORIGINAL_WIDTH = 160;
	private static final int ORIGINAL_HEIGHT = 120;

	private int bufferSize;
	private byte[] videoBuffer;

	private BufferedImage scaled_image;
	private BufferedImage incoming_image;

	private ServerSocket serverSocket;
	private Socket clientSocket;
	private BufferedInputStream videoInputStream;

	private ControlPanel controlPanel;
	private ConnectDialog cDialog;

	private CameraPanel cameraContentPanel;
	
	private boolean awaitingConnection = false;

	public VideoService(ControlPanel parentPanel, ConnectDialog cDialog) {

		this.cDialog = cDialog;
		this.controlPanel = parentPanel;
		cameraContentPanel = new CameraPanel();
		scaled_image = new BufferedImage(OUTPUT_WIDTH, OUTPUT_HEIGHT, BufferedImage.TYPE_INT_RGB);
		incoming_image = new BufferedImage(ORIGINAL_WIDTH, ORIGINAL_HEIGHT, BufferedImage.TYPE_INT_RGB);

		bufferSize = (ORIGINAL_WIDTH * ORIGINAL_HEIGHT) * 2;
		videoBuffer = new byte[bufferSize];
	}

	@Override
	public void run() {
		cDialog.textArea.append("Starting Video Service...");
		createInternalFrame();
		createServer(); // blocking call socket.accept();
		cDialog.textArea.append("Video Service connected.");
		fetchFrames(); // blocking call while loop

	}

	private void fetchFrames() {
		while (true) {
			synchronized (this) {
				try {
					int offset = 0;
					while (offset < bufferSize) {
						offset += videoInputStream.read(videoBuffer, offset, bufferSize - offset);
					}
					for (int i = 0; i < bufferSize; i += 4) {
						int y1 = videoBuffer[i] & 0xFF;
						int y2 = videoBuffer[i + 2] & 0xFF;
						int u = videoBuffer[i + 1] & 0xFF;
						int v = videoBuffer[i + 3] & 0xFF;
						int rgb1 = convertYUVtoARGB(y1, u, v);
						int rgb2 = convertYUVtoARGB(y2, u, v);
						incoming_image.setRGB((i % (ORIGINAL_WIDTH * 2)) / 2, i / (ORIGINAL_WIDTH * 2), rgb1);
						incoming_image.setRGB((i % (ORIGINAL_WIDTH * 2)) / 2 + 1, i / (ORIGINAL_WIDTH * 2), rgb2);

					}
				} catch (Exception e) {
					break;
				}
			}
			scaled_image = scaleImage(incoming_image, 4.0f);
			cameraContentPanel.repaint(1);
		}
	}

	private void createServer() {
		try {
			serverSocket = new ServerSocket(PORT);
			awaitingConnection = true;
			clientSocket = serverSocket.accept();
			videoInputStream = new BufferedInputStream(clientSocket.getInputStream());
			awaitingConnection = false;
		} catch (IOException e) {
			cDialog.textArea.append("Error creating Video Service: " + e.getMessage());
		}
	}

	private void createInternalFrame() {
		controlPanel.videoFrame = new JInternalFrame("Camera View");
		controlPanel.videoFrame.getContentPane().add(cameraContentPanel);
		controlPanel.videoFrame.setPreferredSize(new Dimension(OUTPUT_WIDTH, OUTPUT_HEIGHT));
		controlPanel.videoFrame.setDefaultCloseOperation(JInternalFrame.DISPOSE_ON_CLOSE);
		controlPanel.videoFrame.pack(); // TODO: sure?
		controlPanel.videoFrame.setVisible(true);
	}

	private int convertYUVtoARGB(int y, int u, int v) {
		int c = y - 16;
		int d = u - 128;
		int e = v - 128;
		int r = (298 * c + 409 * e + 128) / 256;
		int g = (298 * c - 100 * d - 208 * e + 128) / 256;
		int b = (298 * c + 516 * d + 128) / 256;
		r = r > 255 ? 255 : r < 0 ? 0 : r;
		g = g > 255 ? 255 : g < 0 ? 0 : g;
		b = b > 255 ? 255 : b < 0 ? 0 : b;
		return 0xff000000 | (r << 16) | (g << 8) | b;
	}

	BufferedImage scaleImage(BufferedImage image, float factor) {
		int w = (int) (image.getWidth() * factor);
		int h = (int) (image.getHeight() * factor);
		BufferedImage after = new BufferedImage(w, h, BufferedImage.TYPE_INT_ARGB);
		AffineTransform at = new AffineTransform();
		at.scale(factor, factor);
		AffineTransformOp scaleOp = new AffineTransformOp(at, AffineTransformOp.TYPE_BILINEAR);
		after = scaleOp.filter(image, after);
		return after;
	}

	public void close() {
		try {
			if (videoInputStream != null)
				videoInputStream.close();
			if (clientSocket != null)
				clientSocket.close();
			if (serverSocket != null)
				serverSocket.close();
		} catch (Exception e1) {
			cDialog.textArea.append("Exception closing window: " + e1);
		}
	}
	
	public synchronized boolean isAwaitingConnection() {
		return this.awaitingConnection;
	}

	class CameraPanel extends JPanel {
		private static final long serialVersionUID = 1L;

		@Override
		protected void paintComponent(Graphics g) {
			super.paintComponent(g);
			// Ensure that we don't paint while the image is being refreshed
			synchronized (VideoService.this) {
				g.drawImage(scaled_image, 0, 0, null);
			}
		}
	}

}
