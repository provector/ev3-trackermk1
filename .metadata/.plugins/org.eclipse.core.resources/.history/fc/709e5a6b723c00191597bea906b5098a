package provector.trackerMk1.pc;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import java.awt.BorderLayout;
import javax.swing.border.TitledBorder;
import java.awt.Dimension;
import net.miginfocom.swing.MigLayout;
import javax.swing.JButton;
import javax.swing.JSlider;
import javax.swing.SwingConstants;
import javax.swing.JLabel;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JProgressBar;
import javax.swing.JTextField;
import javax.swing.JInternalFrame;

public class ControlPanel {

	private JFrame frame;
	private JTextField batteryVoltageField;
	private JTextField batteryCurrentField;
	protected JInternalFrame videoFrame;
	private BrickControler controlerDialog;

	/**
	 * Create the application.
	 */
	public ControlPanel(BrickControler cDialog) {
		this.controlerDialog = cDialog;
		cDialog.log("Creating Control Panel...");
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 1024, 768);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		JPanel controlsPanel = new JPanel();
		controlsPanel.setPreferredSize(new Dimension(10, 210));
		controlsPanel.setBorder(new TitledBorder(null, "Controls", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		frame.getContentPane().add(controlsPanel, BorderLayout.SOUTH);
		controlsPanel.setLayout(null);
		
		JButton fwdButton = new JButton("FWD");
		fwdButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		fwdButton.setBounds(472, 40, 76, 64);
		controlsPanel.add(fwdButton);
		
		JSlider cameraSpeedSlider = new JSlider();
		cameraSpeedSlider.setBounds(394, 12, 232, 16);
		controlsPanel.add(cameraSpeedSlider);
		
		JButton revButton = new JButton("REV");
		revButton.setBounds(472, 105, 76, 64);
		controlsPanel.add(revButton);
		
		JButton leftButton = new JButton("<---");
		leftButton.setBounds(394, 105, 76, 64);
		controlsPanel.add(leftButton);
		
		JButton rightButton = new JButton("--->\n");
		rightButton.setBounds(550, 105, 76, 64);
		controlsPanel.add(rightButton);
		
		JButton cameraLeftButton = new JButton("<- C");
		cameraLeftButton.setBounds(394, 40, 76, 25);
		controlsPanel.add(cameraLeftButton);
		
		JButton cameraRightButton = new JButton("C ->");
		cameraRightButton.setBounds(550, 40, 76, 25);
		controlsPanel.add(cameraRightButton);
		
		JSlider motorsSpeedSlider = new JSlider();
		motorsSpeedSlider.setOrientation(SwingConstants.VERTICAL);
		motorsSpeedSlider.setBounds(356, 12, 26, 188);
		controlsPanel.add(motorsSpeedSlider);
		
		JSlider motorsAccSlider = new JSlider();
		motorsAccSlider.setOrientation(SwingConstants.VERTICAL);
		motorsAccSlider.setBounds(638, 12, 26, 188);
		controlsPanel.add(motorsAccSlider);
		
		JSlider cameraAccSlider = new JSlider();
		cameraAccSlider.setBounds(394, 184, 232, 16);
		controlsPanel.add(cameraAccSlider);
		
		JProgressBar memoryBar = new JProgressBar();
		memoryBar.setBounds(12, 39, 148, 14);
		controlsPanel.add(memoryBar);
		
		JLabel lblMemory = new JLabel("Memory:");
		lblMemory.setBounds(12, 24, 70, 15);
		controlsPanel.add(lblMemory);
		
		JProgressBar cpuBar = new JProgressBar();
		cpuBar.setBounds(12, 69, 148, 14);
		controlsPanel.add(cpuBar);
		
		JLabel lblCpu = new JLabel("CPU:");
		lblCpu.setBounds(12, 54, 70, 15);
		controlsPanel.add(lblCpu);
		
		JProgressBar motorVoltageBar = new JProgressBar();
		motorVoltageBar.setBounds(12, 100, 148, 14);
		controlsPanel.add(motorVoltageBar);
		
		JLabel lblMotorVoltage = new JLabel("Motor Voltage:");
		lblMotorVoltage.setBounds(12, 85, 126, 15);
		controlsPanel.add(lblMotorVoltage);
		
		batteryVoltageField = new JTextField();
		batteryVoltageField.setEditable(false);
		batteryVoltageField.setBounds(12, 141, 70, 19);
		controlsPanel.add(batteryVoltageField);
		batteryVoltageField.setColumns(10);
		
		JLabel lblBatteryV = new JLabel("Battery V");
		lblBatteryV.setBounds(12, 126, 70, 15);
		controlsPanel.add(lblBatteryV);
		
		JLabel lblBatteryA = new JLabel("Battery A");
		lblBatteryA.setBounds(90, 126, 70, 15);
		controlsPanel.add(lblBatteryA);
		
		batteryCurrentField = new JTextField();
		batteryCurrentField.setEditable(false);
		batteryCurrentField.setColumns(10);
		batteryCurrentField.setBounds(90, 141, 70, 19);
		controlsPanel.add(batteryCurrentField);
		
		JButton disconnectButton = new JButton("Disconnect");
		disconnectButton.setBounds(884, 12, 117, 25);
		controlsPanel.add(disconnectButton);
		
		JButton shutdownButton = new JButton("Shutdown");
		shutdownButton.setBounds(884, 40, 117, 25);
		controlsPanel.add(shutdownButton);
		
		JPanel viewportPanel = new JPanel();
		viewportPanel.setMinimumSize(new Dimension(640, 480));
		viewportPanel.setPreferredSize(new Dimension(640, 480));
		viewportPanel.setBorder(new TitledBorder(null, "Viewport", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		frame.getContentPane().add(viewportPanel, BorderLayout.CENTER);
		viewportPanel.setLayout(new BorderLayout(0, 0));
		
		videoFrame = new JInternalFrame("New JInternalFrame");
		viewportPanel.add(videoFrame, BorderLayout.CENTER);
		videoFrame.setVisible(true);
		
		JPanel leftPanel = new JPanel();
		leftPanel.setPreferredSize(new Dimension(150, 10));
		leftPanel.setBorder(new TitledBorder(null, "Left Panel", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		frame.getContentPane().add(leftPanel, BorderLayout.WEST);
		
		JPanel rightPanel = new JPanel();
		rightPanel.setPreferredSize(new Dimension(150, 10));
		rightPanel.setBorder(new TitledBorder(null, "Right", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		frame.getContentPane().add(rightPanel, BorderLayout.EAST);
	}
}
