package provector.trackerMk1.shared.entities;

public enum Command {
	/*
	 * Unused for now
	 */
	FORWARD_GO, 
	FORWARD_STOP, 
	BACKWARD_GO, 
	BACKWARD_STOP, 
	LEFT_TURN_GO,
	LEFT_TURN_STOP, 
	RIGHT_TURN_GO, 
	RIGHT_TURN_STOP,
	CAMERA_LEFT_GO,
	CAMERA_LEFT_STOP,
	CAMERA_RIGHT_GO,
	CAMERA_RIGHT_STOP,
	DISCONNECT
	
	/*
	 * START_CAMERA,STOP_CAMERA
	 * START_TELEMETRY,STOP_TELEMETRY
	 */
}
