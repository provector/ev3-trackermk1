package provector.trackerMk1.shared.entities;

public class TelemetryPacket {

	public float batteryCurrent;
	public float motorCurrent;
	public float batteryVoltage;
	
	public long freeMemoryBytes;
	public long totalMemoryBytes;
	
	public int distanceSample;
	
	public double systemLoadaAverage;

	public float getBatteryCurrent() {
		return batteryCurrent;
	}

	public float getMotorCurrent() {
		return motorCurrent;
	}

	public float getBatteryVoltage() {
		return batteryVoltage;
	}

	public long getFreeMemoryBytes() {
		return freeMemoryBytes;
	}

	public long getTotalMemoryBytes() {
		return totalMemoryBytes;
	}

	public int getDistanceSample() {
		return distanceSample;
	}

	public void setBatteryCurrent(float batteryCurrent) {
		this.batteryCurrent = batteryCurrent;
	}

	public void setMotorCurrent(float motorCurrent) {
		this.motorCurrent = motorCurrent;
	}

	public void setBatteryVoltage(float batteryVoltage) {
		this.batteryVoltage = batteryVoltage;
	}

	public void setFreeMemoryBytes(long freeMemoryBytes) {
		this.freeMemoryBytes = freeMemoryBytes;
	}

	public void setTotalMemoryBytes(long totalMemoryBytes) {
		this.totalMemoryBytes = totalMemoryBytes;
	}

	public void setDistanceSample(int distanceSample) {
		this.distanceSample = distanceSample;
	}

	public void setSystemLoadAverage(double systemLoadAverage) {
		this.systemLoadaAverage = systemLoadAverage;
	} 
	
	public double getSystemLoadAverage() {
		return this.systemLoadaAverage;
	}
}
