package provector.trackerMk1.ev3;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.lang.management.ManagementFactory;
import java.lang.management.OperatingSystemMXBean;
import java.net.Socket;


import lejos.hardware.Battery;
import lejos.hardware.sensor.EV3UltrasonicSensor;
import lejos.robotics.SampleProvider;
import lejos.utility.Delay;
import provector.trackerMk1.shared.entities.TelemetryPacket;

public class TelemetryService implements Runnable {

	private EV3UltrasonicSensor ultrasonicSensor;
	
	//private Gson GSON;
	
	private SampleProvider currentDistanceSample;
	private Runtime runtime;
	
	private PrintWriter dataSender;
	private Socket connection;
	private TelemetryPacket currentDataPacket;
	
	private final String LOGSTR = "TelemetryService: ";
	private final long POLL_TIME = TrackerMk1.TELEMETRY_POLL_TIME_MS;
	private final static int T_PORT = 19233;
	private String hostIP;
	private OperatingSystemMXBean operatingSystemMXBean;
	
	private boolean STOP = false;
	
	
	public TelemetryService(String ip) {
		
		operatingSystemMXBean = ManagementFactory.getOperatingSystemMXBean();
		this.hostIP = ip;
		//get Runtime
		runtime = Runtime.getRuntime();
		//get Sensors
		try {
			ultrasonicSensor = new EV3UltrasonicSensor(TrackerMk1.ULTRASONIC_SENSOR_PORT);	
		}catch(Exception e) {
			log("[ERROR]: Ultrasonic sensor unavailable!");
		}
		
		if(ultrasonicSensor!=null && !ultrasonicSensor.isEnabled()) {
			ultrasonicSensor.enable();
		}else {
			log("[WARN]: Ultrasonic sensor was already enabled or is null");
		}
	}
	
	@Override
	public void run() {
		log("[INFO] Starting telemetry service...");
		try {
			connection = new Socket(hostIP,T_PORT);
			dataSender = new PrintWriter(connection.getOutputStream(),true);
		}catch(IOException io) {
			log("[ERROR]: "+io.getMessage());
		}
		//send setting values
		dataSender.println("SETTINGS:"+TrackerMk1.leftMotor.getSpeed()+"#"+TrackerMk1.leftMotor.getMaxSpeed()+"@"+
									  TrackerMk1.rightMotor.getSpeed()+"#"+TrackerMk1.rightMotor.getMaxSpeed()+"@"+
									  TrackerMk1.cameraMotor.getSpeed()+"#"+TrackerMk1.cameraMotor.getMaxSpeed()+"@"+
									  TrackerMk1.leftMotor.getAcceleration()+"#"+TrackerMk1.rightMotor.getAcceleration()+"#"+TrackerMk1.cameraMotor.getAcceleration()+"@"+
									  TrackerMk1.MAX_ACC_VALUE+"#"+TrackerMk1.MAX_LARGE_MOTOR_SPEED+"#"+TrackerMk1.MAX_MEDIUM_MOTOR_SPEED
									  );
		while(!STOP) {
			pollValues();
			sendValues();
			Delay.msDelay(POLL_TIME);
		}
		cleanUp();
		log("[INFO] Telemetry Service thread finished.");
	}
	
	private void cleanUp() {
		try {
			dataSender.close();
			connection.close();
		} catch (IOException e) {
			log("[ERROR]: "+e.getMessage());
		}
		if(ultrasonicSensor!=null && ultrasonicSensor.isEnabled()) {
			ultrasonicSensor.disable();
			ultrasonicSensor.close();
		}else {
			log("[WARN] ultrasonic sensor was already disabled! ");
		}
	}
	
	private void pollValues() {
		
		currentDataPacket = new TelemetryPacket();
		currentDataPacket.setBatteryCurrent(Battery.getBatteryCurrent());
		currentDataPacket.setMotorCurrent(Battery.getMotorCurrent());
		currentDataPacket.setBatteryVoltage(Battery.getVoltage());
		
		currentDataPacket.setFreeMemoryBytes(runtime.freeMemory());
		currentDataPacket.setTotalMemoryBytes(runtime.totalMemory());
		
		currentDataPacket.setDistanceSample(getDistanceCm());
		
		currentDataPacket.setSystemLoadAverage(operatingSystemMXBean.getSystemLoadAverage());
	}
	
	private void sendValues() {
		
		/*Warning: below is same order as above
		 *
		 */
		String dataString = currentDataPacket.getBatteryCurrent()+"@"+
							currentDataPacket.getMotorCurrent()+"@"+
							currentDataPacket.getBatteryVoltage()+"@"+
							currentDataPacket.getFreeMemoryBytes()+"@"+
							currentDataPacket.getTotalMemoryBytes()+"@"+
							currentDataPacket.getDistanceSample()+"@"+
							currentDataPacket.getSystemLoadAverage();
		
		dataSender.println(dataString);
		dataSender.flush();
	}
	
	private int getDistanceCm() {
		
		if(ultrasonicSensor!=null) {
			currentDistanceSample = ultrasonicSensor.getDistanceMode();
			float[] dSample = new float[currentDistanceSample.sampleSize()];
			currentDistanceSample.fetchSample(dSample, 0);
			int distanceCm = (int) (dSample[0]*100);
			if(distanceCm>250) distanceCm = -1;
			return distanceCm;
		}else {
			return -2;
		}
		
	}
	
	public synchronized void sendConfigResponse() {
		dataSender.println("SETTINGS:"+TrackerMk1.leftMotor.getSpeed()+"#"+TrackerMk1.leftMotor.getMaxSpeed()+"@"+
				  TrackerMk1.rightMotor.getSpeed()+"#"+TrackerMk1.rightMotor.getMaxSpeed()+"@"+
				  TrackerMk1.cameraMotor.getSpeed()+"#"+TrackerMk1.cameraMotor.getMaxSpeed()+"@"+
				  TrackerMk1.leftMotor.getAcceleration()+"#"+TrackerMk1.rightMotor.getAcceleration()+"#"+TrackerMk1.cameraMotor.getAcceleration()+"@"+
				  TrackerMk1.MAX_ACC_VALUE+"#"+TrackerMk1.MAX_LARGE_MOTOR_SPEED+"#"+TrackerMk1.MAX_MEDIUM_MOTOR_SPEED
				  );
	}
	
	public void stop() {
		this.STOP = true;
		log("[DEBUG] Stop flag set to true");
	}
	
	private void log(String s) {
		System.out.println(LOGSTR+s);
	}

}
