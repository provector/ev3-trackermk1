package provector.trackerMk1.ev3;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;

import lejos.hardware.Sound;
import lejos.hardware.motor.EV3LargeRegulatedMotor;
import lejos.hardware.motor.EV3MediumRegulatedMotor;
import lejos.hardware.port.MotorPort;
import lejos.hardware.port.Port;
import lejos.hardware.port.SensorPort;
import lejos.utility.Delay;

public class TrackerMk1 {
	
	/*
	 * TODO: [ ] MotorACC in % from config
	 * TODO: [ ] MotorSpeed in % from config
	 * TODO: [ ] Add CPU metrics
	 * TODO: [ ] Add PowerSaving Mode (Sensors not constantly on)
	 * TODO: [?] Make default config class
	 */
	
	/*
	 * constants
	 */
	
	public final static String VERSION = "0.9.7 Alpha (somewhat working).";
	
	private final static int COMMAND_PORT = 19231;
	protected final static int VIDEO_SERVICE_PORT = 19232;
	
	protected final static long TELEMETRY_POLL_TIME_MS = 1000;
	
	protected final static int DEFAULT_VIDEO_WIDTH = 160;
	protected final static int DEFUALT_VIDEO_HEIGHT = 120;
	
	protected final static Port ULTRASONIC_SENSOR_PORT = SensorPort.S2;
	private final static Port LEFT_MOTOR_PORT = MotorPort.D;
	private final static Port RIGHT_MOTOR_PORT = MotorPort.A;
	private final static Port CAMERA_MOTOR_PORT = MotorPort.C;
	
	public final static int MAX_LARGE_MOTOR_SPEED = 960; //opt 960-1020 deg/s
	public final static int MAX_MEDIUM_MOTOR_SPEED = 200; //opt 1440-1500 deg/s lets do 50% for camera movement
	public final static int MAX_ACC_VALUE = 6000; //100%
	
	private final static String LOGSTR = "Main: ";
		
	/*
	 * static variables
	 */
	
	private static ServerSocket commandServer;
	private static Socket commandSocket;
	private static BufferedReader commandReader;
	private static PrintWriter dataSender;
	private static VideoStreamer videoService;
	private static Thread videoServiceThread;
	private static TelemetryService telemetryService;
	private static Thread telemetryServiceThread;
	private static String clientIP;
	
	static EV3LargeRegulatedMotor leftMotor;
	static EV3LargeRegulatedMotor rightMotor;
	static EV3MediumRegulatedMotor cameraMotor; 
	
	private static int mainMotorSpeed;
	private static int mainMotorAcc;
	private static int leftMotorSpeed;
	private static int leftMotorAcc;
	private static int rightMotorSpeed;
	private static int rightMotorAcc;
	private static int cameraMotorSpeed;
	private static int cameraMotorAcc;
	
	/*
	 * loop conditions
	 */
	
	private static boolean mainLoop = true;
	private static boolean RECEIVE_COMMANDS = true;
	
	public static void main(String[] args) {
		
		log("[INFO] Starting TrackerMk1 Firmware ver "+VERSION);
		
		//Start Server
		try {
			commandServer = new ServerSocket(COMMAND_PORT);
		} catch (IOException e) {
			log("[FATAL] Error creating server: "+e.getMessage());
		}
		log("[INFO] Server created on port "+COMMAND_PORT);
		
		log("[INFO] Initiating motors...");
		leftMotor = new EV3LargeRegulatedMotor(LEFT_MOTOR_PORT);
		rightMotor = new EV3LargeRegulatedMotor(RIGHT_MOTOR_PORT);
		cameraMotor = new EV3MediumRegulatedMotor(CAMERA_MOTOR_PORT);	
		
		while(mainLoop) {
			
			
			
			try {
				log("[INFO] Awaiting client connection...");
				commandSocket = commandServer.accept(); //blocking call
				String ipString = commandSocket.getRemoteSocketAddress().toString(); 
				clientIP = ipString.split(":")[0].replace("/","");
				log("[DEBUG] Remote Socket Address (filtered): "+clientIP);
				
			} catch (IOException e) {
				log("[ERROR] While starting command server: "+e.getMessage());
			}
			log("[INFO] Command Server started.");
			
			videoService = new VideoStreamer(clientIP);
			videoServiceThread = new Thread(videoService);
			videoServiceThread.start();
			
			try {
				commandReader = new BufferedReader(new InputStreamReader(commandSocket.getInputStream()));
				dataSender = new PrintWriter(commandSocket.getOutputStream());
			} catch (IOException e) {
				log("[ERROR] Acquiring input stream: "+e.getMessage());
			}
			
			telemetryService = new TelemetryService(clientIP);
			telemetryServiceThread = new Thread(telemetryService);
			telemetryServiceThread.start();
			
			processCommands(); //blocking call (while loop)
			
			//do housekeeping
			
			mainMotorSpeed = 0;
			mainMotorAcc = 0;
			cameraMotorSpeed = 0;
			cameraMotorAcc = 0;
			
			
			stopServices();
			closeClientConnection();
			scheduleGC(1000);			
		
		}//endMAINLOOP

		leftMotor = null;
		rightMotor = null;
		cameraMotor = null;
		log("[DEBUG] Variables nulled");
		
		log("[INFO] Stopping services... Video");
		if(videoService!=null&&videoServiceThread.isAlive()) {
			videoService.stop();
			while(videoServiceThread.isAlive()) {
				Delay.msDelay(100);
			}
		}else {
			log("[WARN] Video service already stopped.");
		}
		videoServiceThread = null;
		videoService = null;
		log("[INFO] Video done.");
		log("[INFO] Stopping services... Telemetry");
		telemetryService.stop();
		if(telemetryService!=null&&telemetryServiceThread.isAlive()) {
			while(telemetryServiceThread.isAlive()) {
				Delay.msDelay(100);
			}
		}else {
			log("[WARN] Telemetry service already stopped.");
		}
		telemetryServiceThread = null;
		telemetryService = null;
		log("[INFO] Telemetry done.");
		log("[INFO] Services stopped.");
		
		log("[INFO] Closing command server...");
		try {
			commandServer.close();
		
		}catch(IOException io) {
			log("[ERROR] While closing server: "+io.getMessage());
		}
		commandServer = null;
		log("[INFO] Command server closed");
		log("[INFO] Closing main Program");
		
	}//endMain
	
	

	private static void processCommands() {
		String currentLine = null;
		
		//TODO: create separate method
		while(RECEIVE_COMMANDS) {
			try {
				currentLine=commandReader.readLine();
				log("REEAD LINE: "+currentLine);
			}catch(IOException io) {
				log("[ERROR] IO Error on command pipeline: "+io.getMessage());
				RECEIVE_COMMANDS = false;
				break;
			}
			System.out.println("LINE: "+currentLine);
			//process multi-part commands
			if(currentLine.startsWith("CONFIG")) {
				String[] configArr = currentLine.split(":");
				if(configArr.length==3) {
					String key = configArr[1];
					String value = configArr[2];
					
					switch(key) {
					case	"MM_SPEED"	:
										setMainMotorSpeed(value);
										break;
					case	"MM_ACC"	:
										setMainMotorAcc(value);
										break;
					case	"L_SPEED"	:
										setLeftMotorSpeed(value);
										break;
					case	"R_SPEED"	:
										setRightMotorSpeed(value);
										break;
					case	"L_ACC"		:
										setLeftMotorAcc(value);
										break;
					case	"R_ACC"		:
										setRightMotorAcc(value);
										break;
					case 	"CM_SPEED"	:
										setCameraMotorSpeed(value);
										break;
					case	"CM_ACC"	:
										setCameraMotorAcc(value);
 										break;
					case	"REQUEST"	:	
										telemetryService.sendConfigResponse();
										break;
					default:
						log("[WARN] Unrecognized config key: "+key);
					}//endSWITCH
					
				}else{
					log("[WARN] Inconsistent config data ("+currentLine+"), skipping...");
					continue;
				}//endIF
				
			}else{
				
				//process single commands
				switch(currentLine) {
				case "MOTORS_STOP"		:
										motorsStop();
										break;
				case "FORWARD_GO"		:
										motorsForward();
										break;										
				case "FORWARD_STOP"		:	
										motorsStop();
										break;
				case "BACKWARD_GO"		:		
										motorsBackwards();
										break;
				case "BACKWARD_STOP"	:
										motorsStop();
										break;
				case "LEFT_TURN_GO"		:
										motorsLeft();
										break;
				case "LEFT_TURN_STOP"	:	
										motorsStop();
										break;
				case "RIGHT_TURN_GO"	:	
										motorsRight();
										break;
				case "RIGHT_TURN_STOP"	:
										motorsStop();
										break;
				case "CAMERA_LEFT_GO"	:	
										cameraMotorLeft();
										break;
				case "CAMERA_LEFT_STOP"	:
										cameraMotorStop();
										break;
				case "CAMERA_RIGHT_GO"	:
										cameraMotorRight();
										break;
				case "CAMERA_RIGHT_STOP":
										cameraMotorStop();
										break;
				case "DISCONNECT"		:
										triggerDisconnect();
										break;
				case "SHUTDOWN"			:
										triggerShutdown();
										break;
				case "SHOOT"			:
										Sound.beep();
										break;
				default:
					log("[WARN] Unrecognized command!: "+currentLine);
				}//endSWTICH
			}//endIFStartsWith
								
		}//whileRECEIVE_COMMANDS
	}
	
	private static void scheduleGC(long delay) {
		Runtime.getRuntime().gc();
		Delay.msDelay(delay);
	}
	
	private static void stopServices() {
		telemetryService.stop();
		while(telemetryServiceThread.isAlive()) {
			
		}
		log("[INFO] Telemetry Service stopped.");
		videoService.stop();
		while(videoServiceThread.isAlive()) {
			
		}
		log("[INFO] Video Service stopped.");
	}
	
	private static void triggerDisconnect() {
		log("[INFO] Triggering disconnect.");
		RECEIVE_COMMANDS=false;
		
	}
	
	private static void triggerShutdown() {
		log("[INFO] Triggering shutdown.");
		mainLoop = false;
		RECEIVE_COMMANDS=false;
	}
			
	private static void closeClientConnection() {
		log("[INFO] Closing client connection...");
		try {
			dataSender.close();
			commandReader.close();
			commandSocket.close();
		}catch(IOException io) {
			log("[ERROR] when closing client connection: "+io.getMessage());
		}
		dataSender = null;
		commandReader = null;
		commandSocket = null;
		clientIP = null;
		log("[INFO] Client connection closed.");
	}
	
	private static void setMainMotorSpeed(String configValue) {
		try {
			int speed = Integer.parseInt(configValue);
			setMainMotorSpeed(speed);
		}catch(NumberFormatException n) {
			log("[ERROR] Incorrect value: "+configValue);
			return;
		}
	}
	
	private static void setLeftMotorSpeed(String configValue) {
		try {
			int speed = Integer.parseInt(configValue);
			setLeftMotorSpeed(speed);
		}catch(NumberFormatException n) {
			log("[ERROR] Incorrect value: "+configValue);
			return;
		}
	}
	
	private static void setRightMotorSpeed(String configValue) {
		try {
			int speed = Integer.parseInt(configValue);
			setRightMotorSpeed(speed);
		}catch(NumberFormatException n) {
			log("[ERROR] Incorrect value: "+configValue);
			return;
		}
	}
	
	private static void setLeftMotorSpeed(int speed) {
		if(speed<0) {
			speed = 0;
			log("[WARN] Left motor speed value "+speed+" over minimum threshold, setting to min");
		}
		if(speed>MAX_LARGE_MOTOR_SPEED) {
			leftMotorSpeed = MAX_LARGE_MOTOR_SPEED;
			log("[WARN] Left motor speed value "+speed+" over maximum threshold, setting max");
		}else {
			leftMotorSpeed = speed;
		}
		leftMotor.setSpeed(leftMotorSpeed);
	}
	
	private static void setRightMotorSpeed(int speed) {
		if(speed<0) {
			speed = 0;
			log("[WARN] Large motor speed value "+speed+" over minimum threshold, setting to min");
		}
		if(speed>MAX_LARGE_MOTOR_SPEED) {
			rightMotorSpeed = MAX_LARGE_MOTOR_SPEED;
			log("[WARN] Large motor speed value "+speed+" over maximum threshold, setting max");
		}else {
			rightMotorSpeed = speed;
		}
		rightMotor.setSpeed(rightMotorSpeed);
	}
	
	private static void setMainMotorSpeed(int speed) {
		if(speed<0) {
			speed = 0;
			log("[WARN] Large motor speed value "+speed+" over minimum threshold, setting to min");
		}
		if(speed>MAX_LARGE_MOTOR_SPEED) {
			mainMotorSpeed = MAX_LARGE_MOTOR_SPEED;
			log("[WARN] Large motor speed value "+speed+" over maximum threshold, setting max");
		}else {
			mainMotorSpeed = speed;
		}
		leftMotor.setSpeed(mainMotorSpeed);
		rightMotor.setSpeed(mainMotorSpeed);
	}
	
	private static void setMainMotorAcc(String configValue) {
		try {
			int acc = Integer.parseInt(configValue);
			setMainMotorAcc(acc);
		}catch(NumberFormatException n) {
			log("[ERROR] Incorrect value: "+configValue);
			return;
		}
	}
	
	private static void setLeftMotorAcc(String configValue) {
		try {
			int acc = Integer.parseInt(configValue);
			setLeftMotorAcc(acc);
		}catch(NumberFormatException n) {
			log("[ERROR] Incorrect value: "+configValue);
			return;
		}
	}
	
	
	private static void setLeftMotorAcc(int accValue) {
		if(accValue<0) {
			accValue = 0;
			log("[WARN] Left motor acceleration value "+accValue+" over minimum threshold, setting to min");
		}		
		if(accValue>MAX_ACC_VALUE) {
			leftMotorAcc = MAX_ACC_VALUE;
			log("[WARN] Left motor acceleration value "+accValue+" over maximum threshold, setting max");
		}else {
			leftMotorAcc = accValue;
		}
		leftMotor.setAcceleration(leftMotorAcc);
	}
	
	private static void setRightMotorAcc(String configValue) {
		try {
			int acc = Integer.parseInt(configValue);
			setRightMotorAcc(acc);
		}catch(NumberFormatException n) {
			log("[ERROR] Incorrect value: "+configValue);
			return;
		}
	}
	
	private static void setRightMotorAcc(int accValue) {
		if(accValue<0) {
			accValue = 0;
			log("[WARN] Right motor acceleration value "+accValue+" over minimum threshold, setting to min");
		}		
		if(accValue>MAX_ACC_VALUE) {
			rightMotorAcc = MAX_ACC_VALUE;
			log("[WARN] Right motor acceleration value "+accValue+" over maximum threshold, setting max");
		}else {
			rightMotorAcc = accValue;
		}
		rightMotor.setAcceleration(rightMotorAcc);
	}
	
	private static void setMainMotorAcc(int accValue) {
		if(accValue<0) {
			accValue = 0;
			log("[WARN] Large motor acceleration value "+accValue+" over minimum threshold, setting to min");
		}		
		if(accValue>MAX_ACC_VALUE) {
			mainMotorAcc = MAX_ACC_VALUE;
			log("[WARN] Large motor acceleration value "+accValue+" over maximum threshold, setting max");
		}else {
			mainMotorAcc = accValue;
		}
		leftMotor.setAcceleration(mainMotorAcc);
		rightMotor.setAcceleration(mainMotorAcc);
	}
	
	private static void setCameraMotorSpeed(String configValue) {
		try {
			int speed = Integer.parseInt(configValue);
			setCameraMotorSpeed(speed);
		}catch(NumberFormatException n) {
			log("[ERROR] Incorrect value: "+configValue);
			return;
		}
	}
	
	private static void setCameraMotorSpeed(int speed) {
		if(speed<0) {
			speed = 0;
			log("[WARN] Medium motor speed value "+speed+" over minimum threshold, setting to min");
		}
		if(speed>MAX_MEDIUM_MOTOR_SPEED) {
			cameraMotorSpeed = MAX_MEDIUM_MOTOR_SPEED;
			log("[WARN] Medium motor speed value "+speed+" over maximum threshold, setting max");
		}else {
			cameraMotorSpeed = speed;
		}
		cameraMotor.setSpeed(cameraMotorSpeed);
	}
	
	private static void setCameraMotorAcc(String configValue) {
		try {
			int acc = Integer.parseInt(configValue);
			setCameraMotorAcc(acc);
		}catch(NumberFormatException n) {
			log("[ERROR] Incorrect value: "+configValue);
			return;
		}
	}
	
	private static void setCameraMotorAcc(int accValue) {
		if(accValue<0) {
			accValue = 0;
			log("[WARN] Medium motor acceleration value "+accValue+" over minimum threshold, setting to min");
		}	
		if(accValue>MAX_ACC_VALUE) {
			cameraMotorAcc = MAX_ACC_VALUE;
			log("[WARN] Medium motor acceleration value "+accValue+" over maximum threshold, setting max");
		}else {
			cameraMotorAcc = accValue;
		}
		cameraMotor.setAcceleration(cameraMotorAcc);
	}
	
	private static void motorsStop() {
		leftMotor.stop(true);
		rightMotor.stop(true);
	}
	
	private static void motorsForward() {
		leftMotor.backward();
		rightMotor.backward();
	}
	
	private static void motorsBackwards() {
		leftMotor.forward();
		rightMotor.forward();
	}
	
	private static void motorsLeft() {
		rightMotor.backward();
		leftMotor.forward();
	}
	
	private static void motorsRight() {
		rightMotor.forward();
		leftMotor.backward();
	}
	
	private static void cameraMotorLeft() {
		cameraMotor.forward();
	}
	
	private static void cameraMotorRight() {
		cameraMotor.backward();
	}
	
	private static void cameraMotorStop() {
		cameraMotor.flt(false);
	}
	
	private static void log(String s) {
		System.out.println(LOGSTR+s);
	}
	
}
