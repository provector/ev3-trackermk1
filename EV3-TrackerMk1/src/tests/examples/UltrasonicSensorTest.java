package tests.examples;

import java.util.Arrays;

import lejos.hardware.BrickFinder;
import lejos.hardware.port.SensorPort;
import lejos.hardware.sensor.EV3UltrasonicSensor;
import lejos.robotics.SampleProvider;
import lejos.utility.Delay;

public class UltrasonicSensorTest {
	
	public static void main(String[] args) {
		
		EV3UltrasonicSensor us = new EV3UltrasonicSensor(SensorPort.S2);
		us.enable();
		for(int i=0;i<3;i++) {
			SampleProvider s = us.getDistanceMode();
			float[] dSample = new float[s.sampleSize()];
			s.fetchSample(dSample, 0);
			float sample = dSample[0];
			int distanceCm = (int) (sample*100);
			System.out.println("raw: "+dSample[0]+", "+distanceCm+" cm");
			Delay.msDelay(1000);
		}
		us.disable();

	}
	
}
