package provector.trackerMk1.shared.entities;

public class BrickSettings {

	public int leftMotorSpeed;
	public int rightMotorSpeed;
	public int camMotorSpeed;
	
	public int leftMotorAcc;
	public int rightMotorAcc;
	public int camMotorAcc;
	
	public int motorAccMaxHardcoded;
	
	public float leftMotorReportedMaxSpeed;
	public float rightMotorReportedMaxSpeed;
	public float camMotorReportedMaxSpeed;
	
	public int largeMotorMaxHardcodedSpeed;
	public int mediumMotorMaxHardcodedSpeed;
	
	public int getLeftMotorSpeed() {
		return leftMotorSpeed;
	}
	public void setLeftMotorSpeed(int leftMotorSpeed) {
		this.leftMotorSpeed = leftMotorSpeed;
	}
	public int getRightMotorSpeed() {
		return rightMotorSpeed;
	}
	public void setRightMotorSpeed(int rightMotorSpeed) {
		this.rightMotorSpeed = rightMotorSpeed;
	}
	public int getCamMotorSpeed() {
		return camMotorSpeed;
	}
	public void setCamMotorSpeed(int camMotorSpeed) {
		this.camMotorSpeed = camMotorSpeed;
	}
	public int getLeftMotorAcc() {
		return leftMotorAcc;
	}
	public void setLeftMotorAcc(int leftMotorAcc) {
		this.leftMotorAcc = leftMotorAcc;
	}
	public int getRightMotorAcc() {
		return rightMotorAcc;
	}
	public void setRightMotorAcc(int rightMotorAcc) {
		this.rightMotorAcc = rightMotorAcc;
	}
	public int getCamMotorAcc() {
		return camMotorAcc;
	}
	public void setCamMotorAcc(int camMotorAcc) {
		this.camMotorAcc = camMotorAcc;
	}
	public int getMotorAccMaxHardcoded() {
		return motorAccMaxHardcoded;
	}
	public void setMotorAccMaxHardcoded(int motorAccMaxHardcoded) {
		this.motorAccMaxHardcoded = motorAccMaxHardcoded;
	}
	public float getLeftMotorReportedMaxSpeed() {
		return leftMotorReportedMaxSpeed;
	}
	public void setLeftMotorReportedMaxSpeed(float leftMotorReportedMaxSpeed) {
		this.leftMotorReportedMaxSpeed = leftMotorReportedMaxSpeed;
	}
	public float getRightMotorReportedMaxSpeed() {
		return rightMotorReportedMaxSpeed;
	}
	public void setRightMotorReportedMaxSpeed(float rightMotorReportedMaxSpeed) {
		this.rightMotorReportedMaxSpeed = rightMotorReportedMaxSpeed;
	}
	public float getCamMotorReportedMaxSpeed() {
		return camMotorReportedMaxSpeed;
	}
	public void setCamMotorReportedMaxSpeed(float camMotorReportedMaxSpeed) {
		this.camMotorReportedMaxSpeed = camMotorReportedMaxSpeed;
	}
	public int getLargeMotorMaxHardcodedSpeed() {
		return largeMotorMaxHardcodedSpeed;
	}
	public void setLargeMotorMaxHardcodedSpeed(int largeMotorMaxHardcodedSpeed) {
		this.largeMotorMaxHardcodedSpeed = largeMotorMaxHardcodedSpeed;
	}
	public int getMediumMotorMaxHardcodedSpeed() {
		return mediumMotorMaxHardcodedSpeed;
	}
	public void setMediumMotorMaxHardcodedSpeed(int mediumMotorMaxHardcodedSpeed) {
		this.mediumMotorMaxHardcodedSpeed = mediumMotorMaxHardcodedSpeed;
	}
	
}
