package provector.trackerMk1.pc;

import java.io.IOException;
import java.io.OutputStream;
import javax.swing.JTextArea;


public class CustomOutputStream extends OutputStream 
{
	private JTextArea outputArea;
	public CustomOutputStream(JTextArea area)
	{
		this.outputArea = area;
	}
	@Override
	public void write(int b) throws IOException
	{
		outputArea.append(String.valueOf((char)b));
	}
}
