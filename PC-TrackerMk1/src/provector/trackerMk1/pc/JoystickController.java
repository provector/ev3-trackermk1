
package provector.trackerMk1.pc;

import java.io.File;

import net.java.games.input.Component;
import net.java.games.input.Component.Identifier;
import net.java.games.input.Controller;
import net.java.games.input.ControllerEnvironment;
import net.java.games.input.Event;
import net.java.games.input.EventQueue;

/**
 * This class shows how to use the event queue system in JInput. It will show
 * how to get the controllers, how to get the event queue for a controller, and
 * how to read and process events from the queue.
 * 
 * @author Endolf
 */
public class JoystickController extends Thread {

	public JoystickController() {
		System.setProperty("net.java.games.input.librarypath", new File("natives").getAbsolutePath());

	}
	
	//TODO: setting controls

	public void run() {
		int cIndex = -1;
		while (true) {
			/* Get the available controllers */
			Controller[] controllers = ControllerEnvironment.getDefaultEnvironment().getControllers();
			if (controllers.length == 0) {
				System.out.println("Found no controllers.");
				System.exit(0);
			} else {
				for (int i = 0; i < controllers.length; i++) {
					String name = controllers[i].getName();
					if (name.contains("Pro Ex")) {
						cIndex = i;
					}
				}
			}
			controllers[cIndex].poll();
			EventQueue queue = controllers[cIndex].getEventQueue();
			Event event = new Event();
			while (queue.getNextEvent(event)) {
				StringBuffer buffer = new StringBuffer();
				float value = event.getValue();
				Component comp = event.getComponent();
				System.out.println(comp.getName() + ":" + event.getValue());
				Identifier id = comp.getIdentifier();
				if(comp.getIdentifier()==Component.Identifier.Button._12) {
					System.exit(0);
				}else
				if(comp.getIdentifier()==Component.Identifier.Axis.POV) {
					if(value==Component.POV.OFF) {
						CLControl.sendCommand("MOTORS_STOP");
					}else 
					if(value==Component.POV.UP) {
						CLControl.sendCommand("FORWARD_GO");
					}else
					if(value==Component.POV.DOWN) {
						CLControl.sendCommand("BACKWARD_GO");
					}else
					if(value==Component.POV.LEFT) {
						CLControl.sendCommand("LEFT_TURN_GO");
					}else
					if(value==Component.POV.RIGHT) {
						CLControl.sendCommand("RIGHT_TURN_GO");
					}		
				}else
				if(comp.getIdentifier()==Component.Identifier.Button._4) {
					if(value==1.0f) {
						CLControl.sendCommand("CAMERA_LEFT_GO");
					}else{
						CLControl.sendCommand("CAMERA_LEFT_STOP");
					}
				}else
				if(comp.getIdentifier()==Component.Identifier.Button._5) {
					if(value==1.0f) {
						CLControl.sendCommand("CAMERA_RIGHT_GO");
					}else{
						CLControl.sendCommand("CAMERA_RIGHT_STOP");
					}
				}else
				if(comp.getIdentifier()==Component.Identifier.Button._1) {
					if(value==1.0f) {
						CLControl.sendCommand("SHOOT");
					}
				}
				
				
			}

		
			try {
				Thread.sleep(20);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	public static void main(String[] args) {
		JoystickController cl = new JoystickController();
		Thread t = new Thread(cl);
		t.start();
	}
}
