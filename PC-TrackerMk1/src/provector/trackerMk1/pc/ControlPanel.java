package provector.trackerMk1.pc;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import java.awt.BorderLayout;
import javax.swing.border.TitledBorder;
import java.awt.Dimension;
import net.miginfocom.swing.MigLayout;
import provector.trackerMk1.shared.entities.BrickSettings;
import provector.trackerMk1.shared.entities.TelemetryPacket;

import javax.swing.JButton;
import javax.swing.JSlider;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.JLabel;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JProgressBar;
import javax.swing.JTextField;
import javax.swing.JInternalFrame;
import javax.swing.UIManager;
import java.awt.Color;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;
import javax.swing.event.ChangeListener;
import javax.swing.event.ChangeEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.border.EtchedBorder;
import javax.swing.JRadioButton;
import javax.swing.JCheckBox;

public class ControlPanel extends Thread{

	private JTextField batteryVoltageField;
	private JTextField batteryCurrentField;
	public JFrame frame;
	private JProgressBar memoryBar;
	private JProgressBar cpuBar;
	private JTextField distanceField;
	JPanel viewportPanel;
	private JSlider leftMotorSpeedSlider;
	private JSlider cameraSpeedSlider;
	private JSlider cameraAccSlider;
	private JSlider leftMotorAccSlider;
	private JSlider rightMotorSpeedSlider;
	private JSlider rightMotorAccSlider;
	private JButton updateButton;
	private JSlider motorSpeedSlider;
	private JSlider motorAccSlider;
	private JTextField motorCurrentField;
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;
	private JTextField textField_3;
	private JTextField textField_4;
	private JTextField textField_5;
	private JTextField textField_6;
	private JTextField textField_7;
	private JTextField textField_8;
	private JTextField textField_9;
	private JTextField textField_10;
	private JTextField textField_11;
	private JTextField textField_12;
	private JTextField textField_13;
	private JTextField textField_14;
	private JTextField textField_15;
	private JTextField textField_16;

	@Override
	public void run() {
		frame.setVisible(true);
	}
	
	/**
	 * Create the application.
	 */
	public ControlPanel() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 1024, 850);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		JPanel controlsPanel = new JPanel();
		controlsPanel.setPreferredSize(new Dimension(10, 275));
		controlsPanel.setBorder(new TitledBorder(null, "Controls", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		frame.getContentPane().add(controlsPanel, BorderLayout.SOUTH);
		controlsPanel.setLayout(null);
		
		JButton fwdButton = new JButton("FWD");
		fwdButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
			}
		});
		fwdButton.setBounds(474, 67, 76, 64);
		controlsPanel.add(fwdButton);
		
		cameraSpeedSlider = new JSlider();
		cameraSpeedSlider.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent e) {
				CLControl.sendCommand("CONFIG:CM_SPEED:"+cameraSpeedSlider.getValue());
			}
		});
		cameraSpeedSlider.setBorder(new TitledBorder(null, "Camera Move Speed", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		cameraSpeedSlider.setBounds(396, 14, 232, 42);
		controlsPanel.add(cameraSpeedSlider);
		
		JButton revButton = new JButton("REV");
		revButton.setBounds(474, 132, 76, 64);
		controlsPanel.add(revButton);
		
		JButton leftButton = new JButton("<---");
		leftButton.setBounds(396, 132, 76, 64);
		controlsPanel.add(leftButton);
		
		JButton rightButton = new JButton("--->\n");
		rightButton.setBounds(552, 132, 76, 64);
		controlsPanel.add(rightButton);
		
		JButton cameraLeftButton = new JButton("<- C");
		cameraLeftButton.setBounds(396, 67, 76, 25);
		controlsPanel.add(cameraLeftButton);
		
		JButton cameraRightButton = new JButton("C ->");
		cameraRightButton.setBounds(552, 67, 76, 25);
		controlsPanel.add(cameraRightButton);
		
		cameraAccSlider = new JSlider();
		cameraAccSlider.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent e) {
				CLControl.sendCommand("CONFIG:CM_ACC:"+cameraAccSlider.getValue());
			}
		});
		cameraAccSlider.setBorder(new TitledBorder(null, "Camera Acceleration", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		cameraAccSlider.setBounds(396, 203, 232, 36);
		controlsPanel.add(cameraAccSlider);
		
		JButton disconnectButton = new JButton("Disconnect");
		disconnectButton.addActionListener(e->{
			CLControl.sendCommand("DISCONNECT");
			System.exit(0);
			
		});
		disconnectButton.setBounds(884, 12, 117, 25);
		controlsPanel.add(disconnectButton);
		
		JButton shutdownButton = new JButton("Shutdown");
		shutdownButton.setBounds(884, 40, 117, 25);
		controlsPanel.add(shutdownButton);
		
		JPanel panel = new JPanel();
		panel.setBorder(new TitledBorder(null, "Motor Speed", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel.setBounds(302, 14, 84, 225);
		controlsPanel.add(panel);
		
		leftMotorSpeedSlider = new JSlider();
		leftMotorSpeedSlider.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent arg0) {
				CLControl.sendCommand("CONFIG:L_SPEED:"+leftMotorSpeedSlider.getValue());
			}
		});
		panel.add(leftMotorSpeedSlider);
		leftMotorSpeedSlider.setBorder(null);
		leftMotorSpeedSlider.setOrientation(SwingConstants.VERTICAL);
		
		rightMotorSpeedSlider = new JSlider();
		rightMotorSpeedSlider.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent e) {
				CLControl.sendCommand("CONFIG:R_SPEED:"+rightMotorSpeedSlider.getValue());
			}
		});
		
		motorSpeedSlider = new JSlider();
		motorSpeedSlider.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent e) {
				int value = motorSpeedSlider.getValue();
				EventQueue.invokeLater(()->{
					leftMotorSpeedSlider.setValue(value);
					rightMotorSpeedSlider.setValue(value);
				});				
				CLControl.sendCommand("CONFIG:MM_SPEED:"+value);

			}
		});
		motorSpeedSlider.setBackground(Color.WHITE);
		motorSpeedSlider.setOrientation(SwingConstants.VERTICAL);
		motorSpeedSlider.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		panel.add(motorSpeedSlider);
		rightMotorSpeedSlider.setOrientation(SwingConstants.VERTICAL);
		rightMotorSpeedSlider.setBorder(null);
		panel.add(rightMotorSpeedSlider);
		
		JPanel panel_1 = new JPanel();
		panel_1.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Motor Acc", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
		panel_1.setBounds(638, 14, 84, 225);
		controlsPanel.add(panel_1);
		
		leftMotorAccSlider = new JSlider();
		leftMotorAccSlider.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent e) {
				CLControl.sendCommand("CONFIG:L_ACC:"+leftMotorAccSlider.getValue());
			}
		});
		panel_1.add(leftMotorAccSlider);
		leftMotorAccSlider.setBorder(null);
		leftMotorAccSlider.setOrientation(SwingConstants.VERTICAL);
		
		rightMotorAccSlider = new JSlider();
		rightMotorAccSlider.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent e) {
				CLControl.sendCommand("CONFIG:R_ACC:"+leftMotorAccSlider.getValue());
			}
		});
		
		motorAccSlider = new JSlider();
		motorAccSlider.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent e) {
				int value = motorAccSlider.getValue();
				EventQueue.invokeLater(()->{
					leftMotorAccSlider.setValue(value);
					rightMotorAccSlider.setValue(value);
				});
				
				CLControl.sendCommand("CONFIG:MM_ACC:"+value);
			}
		});
		motorAccSlider.setBackground(Color.WHITE);
		motorAccSlider.setOrientation(SwingConstants.VERTICAL);
		motorAccSlider.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		panel_1.add(motorAccSlider);
		rightMotorAccSlider.setOrientation(SwingConstants.VERTICAL);
		rightMotorAccSlider.setBorder(null);
		panel_1.add(rightMotorAccSlider);
		
		JLabel lblNewLabel = new JLabel("Left All Right");
		lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel.setBounds(302, 237, 84, 14);
		controlsPanel.add(lblNewLabel);
		
		JLabel lblLeftAllRight = new JLabel("Left All Right");
		lblLeftAllRight.setHorizontalAlignment(SwingConstants.CENTER);
		lblLeftAllRight.setBounds(638, 237, 84, 14);
		controlsPanel.add(lblLeftAllRight);
		
		updateButton = new JButton("Update");
		updateButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				CLControl.sendCommand("CONFIG:REQUEST:0");
			}
		});
		updateButton.setBounds(474, 249, 76, 23);
		controlsPanel.add(updateButton);
		
		JButton btnBeep = new JButton("Beep()");
		btnBeep.setEnabled(false);
		btnBeep.setBounds(732, 14, 89, 23);
		controlsPanel.add(btnBeep);
		
		viewportPanel = new JPanel();
		viewportPanel.setMinimumSize(new Dimension(640, 480));
		viewportPanel.setPreferredSize(new Dimension(640, 540));
		viewportPanel.setBorder(new TitledBorder(null, "Viewport", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		frame.getContentPane().add(viewportPanel, BorderLayout.CENTER);
		viewportPanel.setLayout(new BorderLayout(0, 0));
		
		/*videoFrame = new JInternalFrame("New JInternalFrame");
		viewportPanel.add(videoFrame, BorderLayout.CENTER);
		videoFrame.setVisible(true);*/
		
		JPanel leftPanel = new JPanel();
		leftPanel.setPreferredSize(new Dimension(170, 10));
		leftPanel.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "System Info", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
		frame.getContentPane().add(leftPanel, BorderLayout.WEST);
		leftPanel.setLayout(null);
		
		distanceField = new JTextField();
		distanceField.setBounds(10, 208, 148, 20);
		leftPanel.add(distanceField);
		distanceField.setEditable(false);
		distanceField.setColumns(10);
		
		JLabel lblDistanceCm = new JLabel("Distance CM:");
		lblDistanceCm.setBounds(10, 189, 70, 15);
		leftPanel.add(lblDistanceCm);
		
		JLabel lblBatteryV = new JLabel("Battery V");
		lblBatteryV.setBounds(10, 144, 70, 15);
		leftPanel.add(lblBatteryV);
		
		batteryCurrentField = new JTextField();
		batteryCurrentField.setBounds(88, 159, 70, 19);
		leftPanel.add(batteryCurrentField);
		batteryCurrentField.setEditable(false);
		batteryCurrentField.setColumns(10);
		
		JLabel lblBatteryA = new JLabel("Battery A");
		lblBatteryA.setBounds(88, 144, 70, 15);
		leftPanel.add(lblBatteryA);
		
		batteryVoltageField = new JTextField();
		batteryVoltageField.setBounds(10, 159, 70, 19);
		leftPanel.add(batteryVoltageField);
		batteryVoltageField.setEditable(false);
		batteryVoltageField.setColumns(10);
		
		motorCurrentField = new JTextField();
		motorCurrentField.setBounds(10, 111, 148, 20);
		leftPanel.add(motorCurrentField);
		motorCurrentField.setEditable(false);
		motorCurrentField.setColumns(10);
		
		JLabel lblMotorVoltage = new JLabel("Motor Current");
		lblMotorVoltage.setBounds(10, 92, 126, 15);
		leftPanel.add(lblMotorVoltage);
		
		cpuBar = new JProgressBar();
		cpuBar.setBounds(10, 69, 148, 14);
		leftPanel.add(cpuBar);
		
		memoryBar = new JProgressBar();
		memoryBar.setBounds(10, 39, 148, 14);
		leftPanel.add(memoryBar);
		
		JLabel lblCpu = new JLabel("CPU:");
		lblCpu.setBounds(10, 54, 70, 15);
		leftPanel.add(lblCpu);
		
		JLabel lblMemory = new JLabel("Memory:");
		lblMemory.setBounds(10, 24, 70, 15);
		leftPanel.add(lblMemory);
		
		JLabel lblLeftMotor = new JLabel("Left Motor");
		lblLeftMotor.setEnabled(false);
		lblLeftMotor.setBounds(10, 245, 62, 15);
		leftPanel.add(lblLeftMotor);
		
		textField = new JTextField();
		textField.setEnabled(false);
		textField.setEditable(false);
		textField.setColumns(10);
		textField.setBounds(10, 260, 50, 19);
		leftPanel.add(textField);
		
		JLabel lblRightMotor = new JLabel("Right Motor");
		lblRightMotor.setEnabled(false);
		lblRightMotor.setBounds(108, 245, 62, 15);
		leftPanel.add(lblRightMotor);
		
		textField_1 = new JTextField();
		textField_1.setEnabled(false);
		textField_1.setEditable(false);
		textField_1.setColumns(10);
		textField_1.setBounds(108, 260, 50, 19);
		leftPanel.add(textField_1);
		
		JLabel lblSpeed = new JLabel("Speed");
		lblSpeed.setEnabled(false);
		lblSpeed.setHorizontalAlignment(SwingConstants.CENTER);
		lblSpeed.setBounds(62, 263, 45, 14);
		leftPanel.add(lblSpeed);
		
		textField_2 = new JTextField();
		textField_2.setEnabled(false);
		textField_2.setEditable(false);
		textField_2.setColumns(10);
		textField_2.setBounds(10, 290, 50, 19);
		leftPanel.add(textField_2);
		
		textField_3 = new JTextField();
		textField_3.setEnabled(false);
		textField_3.setEditable(false);
		textField_3.setColumns(10);
		textField_3.setBounds(108, 290, 50, 19);
		leftPanel.add(textField_3);
		
		JLabel lblAcc = new JLabel("Acc");
		lblAcc.setEnabled(false);
		lblAcc.setHorizontalAlignment(SwingConstants.CENTER);
		lblAcc.setBounds(62, 293, 45, 14);
		leftPanel.add(lblAcc);
		
		textField_4 = new JTextField();
		textField_4.setEnabled(false);
		textField_4.setEditable(false);
		textField_4.setColumns(10);
		textField_4.setBounds(10, 400, 50, 19);
		leftPanel.add(textField_4);
		
		JLabel lblCamMotorSpeed = new JLabel("Cam Motor");
		lblCamMotorSpeed.setHorizontalAlignment(SwingConstants.CENTER);
		lblCamMotorSpeed.setEnabled(false);
		lblCamMotorSpeed.setBounds(10, 360, 148, 15);
		leftPanel.add(lblCamMotorSpeed);
		
		JLabel lblSpeed_1 = new JLabel("Speed");
		lblSpeed_1.setHorizontalAlignment(SwingConstants.CENTER);
		lblSpeed_1.setEnabled(false);
		lblSpeed_1.setBounds(10, 380, 50, 15);
		leftPanel.add(lblSpeed_1);
		
		JLabel lblAcc_1 = new JLabel("Acc");
		lblAcc_1.setHorizontalAlignment(SwingConstants.CENTER);
		lblAcc_1.setEnabled(false);
		lblAcc_1.setBounds(108, 380, 50, 15);
		leftPanel.add(lblAcc_1);
		
		textField_5 = new JTextField();
		textField_5.setEnabled(false);
		textField_5.setEditable(false);
		textField_5.setColumns(10);
		textField_5.setBounds(108, 400, 50, 19);
		leftPanel.add(textField_5);
		
		textField_6 = new JTextField();
		textField_6.setBackground(Color.GREEN);
		textField_6.setEnabled(false);
		textField_6.setEditable(false);
		textField_6.setColumns(10);
		textField_6.setBounds(10, 320, 50, 19);
		leftPanel.add(textField_6);
		
		textField_7 = new JTextField();
		textField_7.setBackground(Color.RED);
		textField_7.setEnabled(false);
		textField_7.setEditable(false);
		textField_7.setColumns(10);
		textField_7.setBounds(108, 320, 50, 19);
		leftPanel.add(textField_7);
		
		JLabel lblStall = new JLabel("Stall");
		lblStall.setHorizontalAlignment(SwingConstants.CENTER);
		lblStall.setEnabled(false);
		lblStall.setBounds(62, 323, 45, 14);
		leftPanel.add(lblStall);
		
		textField_8 = new JTextField();
		textField_8.setEnabled(false);
		textField_8.setEditable(false);
		textField_8.setColumns(10);
		textField_8.setBounds(62, 400, 45, 19);
		leftPanel.add(textField_8);
		
		JLabel lblStall_1 = new JLabel("Stall");
		lblStall_1.setHorizontalAlignment(SwingConstants.CENTER);
		lblStall_1.setEnabled(false);
		lblStall_1.setBounds(62, 380, 45, 15);
		leftPanel.add(lblStall_1);
		
		JSpinner spinner = new JSpinner();
		spinner.setBounds(10, 505, 55, 20);
		leftPanel.add(spinner);
		
		JLabel lblPollingRatems = new JLabel("Polling Rate (ms)");
		lblPollingRatems.setBounds(75, 508, 83, 14);
		leftPanel.add(lblPollingRatems);
		
		JPanel rightPanel = new JPanel();
		rightPanel.setPreferredSize(new Dimension(150, 10));
		rightPanel.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Services", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
		frame.getContentPane().add(rightPanel, BorderLayout.EAST);
		rightPanel.setLayout(null);
		
		textField_10 = new JTextField();
		textField_10.setEditable(false);
		textField_10.setColumns(10);
		textField_10.setBackground(Color.ORANGE);
		textField_10.setBounds(20, 261, 20, 20);
		rightPanel.add(textField_10);
		
		JLabel lblTelemetryService = new JLabel("Telemetry Service");
		lblTelemetryService.setBounds(50, 264, 100, 14);
		rightPanel.add(lblTelemetryService);
		
		textField_11 = new JTextField();
		textField_11.setEditable(false);
		textField_11.setColumns(10);
		textField_11.setBackground(Color.GREEN);
		textField_11.setBounds(20, 289, 20, 20);
		rightPanel.add(textField_11);
		
		JLabel lblJoystickController = new JLabel("Joystick Controller");
		lblJoystickController.setBounds(50, 292, 100, 14);
		rightPanel.add(lblJoystickController);
		
		JPanel panel_2 = new JPanel();
		panel_2.setBorder(new TitledBorder(null, "Video Service", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel_2.setBounds(0, 29, 150, 188);
		rightPanel.add(panel_2);
		panel_2.setLayout(null);
		
		textField_9 = new JTextField();
		textField_9.setBounds(10, 22, 20, 20);
		panel_2.add(textField_9);
		textField_9.setBackground(Color.GREEN);
		textField_9.setEditable(false);
		textField_9.setColumns(10);
		
		JLabel lblVideoService = new JLabel("Active (Connected)");
		lblVideoService.setBounds(40, 25, 100, 14);
		panel_2.add(lblVideoService);
		
		JLabel lblNewLabel_1 = new JLabel("Output Video:");
		lblNewLabel_1.setBounds(10, 49, 72, 14);
		panel_2.add(lblNewLabel_1);
		
		textField_12 = new JTextField();
		textField_12.setBounds(10, 68, 53, 20);
		panel_2.add(textField_12);
		textField_12.setColumns(10);
		
		JLabel lblX = new JLabel("x");
		lblX.setBounds(73, 71, 20, 14);
		panel_2.add(lblX);
		
		textField_13 = new JTextField();
		textField_13.setColumns(10);
		textField_13.setBounds(87, 68, 53, 20);
		panel_2.add(textField_13);
		
		JLabel lblInputVideo = new JLabel("Input Video:");
		lblInputVideo.setBounds(10, 96, 72, 14);
		panel_2.add(lblInputVideo);
		
		textField_14 = new JTextField();
		textField_14.setEditable(false);
		textField_14.setColumns(10);
		textField_14.setBounds(10, 115, 53, 20);
		panel_2.add(textField_14);
		
		JLabel label_1 = new JLabel("x");
		label_1.setBounds(73, 118, 20, 14);
		panel_2.add(label_1);
		
		textField_15 = new JTextField();
		textField_15.setEditable(false);
		textField_15.setColumns(10);
		textField_15.setBounds(87, 115, 53, 20);
		panel_2.add(textField_15);
		
		textField_16 = new JTextField();
		textField_16.setEditable(false);
		textField_16.setColumns(10);
		textField_16.setBounds(10, 156, 53, 20);
		panel_2.add(textField_16);
		
		JCheckBox chckbxHud = new JCheckBox("HUD");
		chckbxHud.setBounds(87, 155, 53, 23);
		panel_2.add(chckbxHud);
		
		JLabel lblFps = new JLabel("FPS:");
		lblFps.setBounds(10, 143, 39, 14);
		panel_2.add(lblFps);
	}
	
	public synchronized void updateTelemetry(TelemetryPacket tp) {
	
		memoryBar.setMaximum((int)tp.getTotalMemoryBytes());
		memoryBar.setValue((int)(tp.getTotalMemoryBytes()-tp.getFreeMemoryBytes()));
		
		batteryVoltageField.setText(""+tp.getBatteryVoltage());
		batteryCurrentField.setText(""+tp.getBatteryCurrent());
		
		//TODO: change voltage to current in ui and find approx max
		//System.out.println("mc: "+tp.getMotorCurrent());
		motorCurrentField.setText(""+tp.getMotorCurrent());
		int cpu = (int) (tp.getSystemLoadAverage()*100);
		cpuBar.setValue(cpu);
		int distSample = tp.getDistanceSample();
		if(distSample==-1) {
			distanceField.setText("Inf (>250)");
		}else
			if(distSample==-2) {
				distanceField.setText("Sensor Error!");
			}else {
				distanceField.setText(""+distSample);
			}				
	}
	
	public synchronized void updateSliderValues(BrickSettings s) {
		leftMotorSpeedSlider.setMaximum(s.getLargeMotorMaxHardcodedSpeed());
		rightMotorSpeedSlider.setMaximum(s.getLargeMotorMaxHardcodedSpeed());
		leftMotorSpeedSlider.setValue(s.getLeftMotorSpeed());
		rightMotorSpeedSlider.setValue(s.getRightMotorSpeed());
		leftMotorAccSlider.setMaximum(s.getMotorAccMaxHardcoded());
		leftMotorAccSlider.setValue(s.getLeftMotorAcc());
		rightMotorAccSlider.setMaximum(s.getMotorAccMaxHardcoded());
		rightMotorAccSlider.setValue(s.getRightMotorAcc());
		cameraSpeedSlider.setMaximum(s.getMediumMotorMaxHardcodedSpeed());
		cameraSpeedSlider.setValue(s.getCamMotorSpeed());
		cameraAccSlider.setMaximum(s.getMotorAccMaxHardcoded());
		cameraAccSlider.setValue(s.getCamMotorAcc());
		
		motorSpeedSlider.setMaximum(s.getLargeMotorMaxHardcodedSpeed());
		motorAccSlider.setMaximum(s.getMotorAccMaxHardcoded());
		
		System.out.println("Reported Left: "+s.getLeftMotorReportedMaxSpeed()+" Right: "+s.getRightMotorReportedMaxSpeed()+" Cam: "+s.getCamMotorReportedMaxSpeed());
	}
	
	public synchronized String getDistanceCmString() {
		return distanceField.getText()+" cm";
	}
	
	public synchronized String getLeftMotorSpeedString() {
		return ""+leftMotorSpeedSlider.getValue();
	}
	
	public synchronized String getRightMotorSpeedString() {
		return ""+rightMotorSpeedSlider.getValue();
	}
}
