package provector.trackerMk1.pc;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;

import provector.trackerMk1.shared.entities.BrickSettings;
import provector.trackerMk1.shared.entities.TelemetryPacket;

public class TelemetryReceiver implements Runnable {
	
	final static int T_PORT = 19233;
	
	private boolean connected = false;
	
	private boolean KEEP_GOING = true;
	
	private static TelemetryPacket dataPacket;
	
	public TelemetryReceiver() {
		
		//this.cDialog = cDialog;
	}
	
	@Override
	public void run() {
		log("Starting Telemetry Receiver Service...");
		Socket dataSocket = null;
		BufferedReader dataReader = null;
		ServerSocket tServer = null;
		try {
			tServer = new ServerSocket(T_PORT);
			dataSocket = tServer.accept();
			dataReader = new BufferedReader(new InputStreamReader(dataSocket.getInputStream()));
		}catch(IOException io) {
			log("[ERROR]: "+io.getMessage());
			return;
		}
		log("Telemetry connected, streams acquired");
		
		String incomingData;
		connected = true; 
		while(KEEP_GOING) {
			try {
				incomingData = dataReader.readLine();
			}catch(IOException io) {
				log("[ERROR] While receiving telemtry:"+io.getMessage());
				break;
			}
			if(incomingData==null) {
				log("[WARN] Telemtry received null");
				continue; //TODO: check if its a good ide
			}
			if(incomingData.startsWith("SETTINGS:")) {
				//TODO: error handling
				
				//TODO: set reported max values (lesser one) as motor max in Tracker class
				System.out.println("Receivied settings updating: "+incomingData);
				BrickSettings settings = new BrickSettings();
				incomingData = incomingData.replace("SETTINGS:","");
				String[] dataSets = incomingData.split("@");
				//speeds
				String[] leftMotor = dataSets[0].split("#");
					settings.setLeftMotorSpeed(Integer.parseInt(leftMotor[0]));
					settings.setLeftMotorReportedMaxSpeed(Float.parseFloat(leftMotor[1]));
				String[] rightMotor = dataSets[1].split("#");
					settings.setRightMotorSpeed(Integer.parseInt(rightMotor[0]));
					settings.setRightMotorReportedMaxSpeed(Float.parseFloat(rightMotor[1]));
				String[] camMotor = dataSets[2].split("#");
					settings.setCamMotorSpeed(Integer.parseInt(camMotor[0]));
					settings.setCamMotorReportedMaxSpeed(Float.parseFloat(camMotor[1]));
				String[] accValues = dataSets[3].split("#");
					settings.setLeftMotorAcc(Integer.parseInt(accValues[0]));
					settings.setRightMotorAcc(Integer.parseInt(accValues[1]));
					settings.setCamMotorAcc(Integer.parseInt(accValues[2]));
				String[] hardcodedMax = dataSets[4].split("#");
					settings.setMotorAccMaxHardcoded(Integer.parseInt(hardcodedMax[0]));
					settings.setLargeMotorMaxHardcodedSpeed(Integer.parseInt(hardcodedMax[1]));
					settings.setMediumMotorMaxHardcodedSpeed(Integer.parseInt(hardcodedMax[2]));
				CLControl.getControlPanel().updateSliderValues(settings);
			}else {
				//TODO: add exception handling here
				//do something uisefull with data
				String[] buff = incomingData.split("@");
				dataPacket = new TelemetryPacket();
				dataPacket.setBatteryCurrent(Float.parseFloat(buff[0]));
				dataPacket.setMotorCurrent(Float.parseFloat(buff[1]));
				System.out.println("MC: "+buff[1]);
				dataPacket.setBatteryVoltage(Float.parseFloat(buff[2]));
				dataPacket.setFreeMemoryBytes(Long.parseLong(buff[3]));
				dataPacket.setTotalMemoryBytes(Long.parseLong(buff[4]));
				dataPacket.setDistanceSample(Integer.parseInt(buff[5]));
				dataPacket.setSystemLoadAverage(Double.parseDouble(buff[6]));
				CLControl.getControlPanel().updateTelemetry(dataPacket);
			}
			
			
			
			
		}//endWHILE
		//cleanup
		log("Closing Telemetry Service...");

		try {
			dataReader.close();
			//tServer.close();
		}catch(IOException io) {
			log("[ERROR] While closing telemetry service:"+io.getMessage());
		}
		log("[OK]");
	}
	
	public void stop() {
		this.KEEP_GOING = false;
	}
	
	public synchronized boolean isConnected() {
		return this.connected;
	}
	
	private void log(String s) {
		//cDialog.log(s);
		System.out.println("TELEMETRY: "+s);
	}
}
