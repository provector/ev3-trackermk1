package provector.trackerMk1.pc;

import java.awt.EventQueue;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Scanner;

import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

public class CLControl {
	
	static Console consoleWindow;
	static JoystickController padController;
	static ControlPanel controlPanel;
	static String IP;
	static boolean go = false;
	static PrintWriter commandSender;
	
	public static void main(String[] args) {
		
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (ClassNotFoundException | InstantiationException | IllegalAccessException
				| UnsupportedLookAndFeelException e1) {
			System.out.println("dupa: "+e1.getMessage());
		}
		
		//String brickIP = "192.168.1.7";
		String brickIP = "10.0.1.1";
		int brickPort = 19231;
		
		//EventQueue.invokeLater(new Runnable() {
		//	public void run() {
				consoleWindow = new Console();
				consoleWindow.setVisible(true);
		//	}
		//});
		while(consoleWindow==null||!consoleWindow.isVisible()) {
			
		}
		System.out.println("waiting for go");
		while(!go) {
			sleep(1);
		}
		if(IP!=null&&IP.isEmpty()==false) {
			System.out.println("Setting custom IP");
			brickIP = IP;
		}
		
		CustomOutputStream redirect = new CustomOutputStream(consoleWindow.textArea);
		System.setOut(new PrintStream(redirect));
		controlPanel = new ControlPanel();
		controlPanel.setDaemon(true);
		controlPanel.start();
		while(controlPanel.isAlive()==false) {
			sleep(1);
		}
		//Start Video service for incoming connections
		VideoService videoService = new VideoService();
		log("Starting video server.");
		Thread videoServiceThread = new Thread(videoService);
		videoServiceThread.setDaemon(true);
		videoServiceThread.start();
		while(!videoService.isAwaitingConnection()) {
		
		}
		log("Video server started.");
		TelemetryReceiver telemetryReceiver = new TelemetryReceiver();
		Thread telemetryServiceThread = new Thread(telemetryReceiver);
		telemetryServiceThread.start();
		
		//Connect to Brick
		Socket connectionSocket = null;
		
		BufferedReader dataReceiver = null;
		try {
			connectionSocket = new Socket(brickIP,brickPort);
			commandSender = new PrintWriter(connectionSocket.getOutputStream(),true);
			
		} catch (IOException e) {
			log("[ERROR]: "+e.getMessage());
			videoService.interruptAwait();
			return;
		}
		
		padController = new JoystickController();
		padController.setDaemon(true);
		padController.start();
		log("[INFO] Pad Controller running.");
		
		
		boolean stop = false;
		log("running");
		Scanner sc = new Scanner(System.in);
		String keyboard;
		while((keyboard=sc.nextLine())!=null) {
			if(keyboard.trim().toLowerCase().equals("q")) break;
			commandSender.println(keyboard);
			commandSender.flush();
			System.out.println("command sent: "+keyboard);
		}
		try {
			commandSender.close();
			connectionSocket.close();
		}catch(IOException io) {
			io.printStackTrace();
		}
	
		log("done.");
	}
	
	synchronized static public void go(String ip) {
		IP = ip;
		go = true;
	}
	
	private static void log(String s) {
		System.out.println("MAIN: "+s);
	}
	
	private static void sleep(long ms) {
		try {
			Thread.sleep(ms);
		}catch(InterruptedException e) {
			System.out.println("Thread access violation: "+e.getMessage());
		}
	}
	
	public static ControlPanel getControlPanel() {
		return controlPanel;
	}
	
	public static synchronized void sendCommand(String cmd) {
		System.out.println("[DEBUG] Sending command: "+cmd);
		commandSender.println(cmd);
	}
}
