package tests.examples;

import java.awt.BorderLayout;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.net.Socket;
import java.net.UnknownHostException;
 
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.SwingUtilities;

import net.miginfocom.swing.MigLayout;
import java.awt.Window.Type;
 
/**
/**
 * This was written on jCrete 2015 on a LeJOS session by Steven Chin.
 * 
 * We were given the task to build a lego and do something cool with it.
 * We built a pretty large tank and remote-controlled it via wi-fi with
 * simple socket commands.
 * 
 * For more info, go to https://mihail.stoynov.com/?p=2909
 * 
 * This is a mock server for testing if you don't have a lego at hand.
 * 
 * @author Nayden Gochev, jug.bg
 * @author Mihail Stoynov, jug.bg
 *
 */
public class Ev3RemoteControlClient extends JFrame {
 
	private static final long serialVersionUID = -8402983606638099877L;
 
	private final JButton left;
	private final JButton right;
	private final JButton up;
	private final JButton down;
	private final JButton stop;
 
	private BufferedWriter pw;
	private JButton camRight;
	private JButton camLeft;
 
	public static void main(String[] args) {
		
		Ev3VideoStreamTestApp_PC cameraFrame = new Ev3VideoStreamTestApp_PC();
		Thread cameraThread = new Thread(cameraFrame);
		cameraThread.start();
		
        System.out.println("Video server Initialized");
		Ev3RemoteControlClient remote = new Ev3RemoteControlClient();
		remote.setSize(500, 500);
		remote.setVisible(true);
	}
 
	public Ev3RemoteControlClient() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		try {
			System.out.println("Connecting...");
			Socket socket = new Socket("192.168.1.7", 19231);
//			Socket socket = new Socket("127.0.0.1", 19231);//for mocking
			pw = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
			System.out.println("Done.");
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		getContentPane().setLayout(new MigLayout("", "[66px][71px][75px]", "[25px][25px][25px]"));
		getContentPane().setLayout(new MigLayout("", "[1px]", "[1px][][]"));
		
		camLeft = new JButton("<- Cam");
		getContentPane().add(camLeft, "flowx,cell 0 0,grow");
		camLeft.addMouseListener(new MouseAdapter() {
			 
			@Override
			public void mouseReleased(MouseEvent e) {
				camLeftRelease();
//				System.out.println("LEFT-RELEASE\n");pw.flush();
			}
 
 
			@Override
			public void mousePressed(MouseEvent e) {
				camLeftPress();
//				System.out.println("LEFT-PRESS\n");pw.flush();
			}
		});	
		
		camRight = new JButton("Cam ->");
		camRight.addMouseListener(new MouseAdapter() {
			 
			@Override
			public void mouseReleased(MouseEvent e) {
				camRightRelease();
//				System.out.println("LEFT-RELEASE\n");pw.flush();
			}
 
 
			@Override
			public void mousePressed(MouseEvent e) {
				camRightPress();
//				System.out.println("LEFT-PRESS\n");pw.flush();
			}
		});	
		up = new JButton("UP");
		this.getContentPane().add(up, "cell 0 0,grow");
		up.addMouseListener(new MouseAdapter() {
 
			@Override
			public void mouseReleased(MouseEvent e) {
				upRelease();
//				System.out.println("UP-RELEASE\n");pw.flush();
			}
 
 
 
			@Override
			public void mousePressed(MouseEvent e) {
				upPress();
//				System.out.println("UP-PRESS\n");pw.flush();
			}
 
 
		});
		getContentPane().add(camRight, "cell 0 0,grow");
		
		
		
		left = new JButton("LEFT");
		this.getContentPane().add(left, "flowx,cell 0 1,grow");
		down = new JButton("DOWN");
		this.getContentPane().add(down, "cell 0 1,grow");
		right = new JButton("RIGHT");
		this.getContentPane().add(right, "cell 0 1,grow");
		stop = new JButton("STOP\n");
		this.getContentPane().add(stop, "cell 0 2,grow");
		stop.addMouseListener(new MouseAdapter() {
 
			@Override
			public void mousePressed(MouseEvent e) {
				sendCommand("STOP");
			}
		});
		
		//keypad arrows work only if the focus is on the stop button
		//lame but that's life
		stop.addKeyListener(new KeyListener() {
			@Override
			public void keyTyped(KeyEvent event) {
				// TODO Auto-generated method stub
		
			}
		
			@Override
			public void keyReleased(KeyEvent event) {
				if(KeyEvent.VK_UP == event.getKeyCode()){
					upRelease();
				} else if(KeyEvent.VK_DOWN == event.getKeyCode()){
					downRelease();
				} else if(KeyEvent.VK_LEFT == event.getKeyCode()){
					leftRelease();
				} else if(KeyEvent.VK_RIGHT == event.getKeyCode()){
					rightRelease();
				} else if(KeyEvent.VK_DELETE == event.getKeyChar()) {
					camLeftRelease();
				} else if(KeyEvent.VK_PAGE_DOWN == event.getKeyChar()) {
					camRightRelease();
				}
			}
		
			@Override
			public void keyPressed(KeyEvent event) {
				if(KeyEvent.VK_UP == event.getKeyCode()){
					upPress();
				} else if(KeyEvent.VK_DOWN == event.getKeyCode()){
					downPress();
				} else if(KeyEvent.VK_LEFT == event.getKeyCode()){
					leftPress();
				} else if(KeyEvent.VK_RIGHT == event.getKeyCode()){
					rightPress();
				} else if(KeyEvent.VK_DELETE == event.getKeyChar()) {
					camLeftPress();
				} else if(KeyEvent.VK_PAGE_DOWN == event.getKeyChar()) {
					camRightPress();
				}
			}
		});
		right.addMouseListener(new MouseAdapter() {
 
			@Override
			public void mouseReleased(MouseEvent e) {
				rightRelease();
//				System.out.println("RIGHT-RELEASE\n");pw.flush();
			}
 
 
 
			@Override
			public void mousePressed(MouseEvent e) {
				rightPress();
//				System.out.println("RIGHT-PRESS\n");pw.flush();
			}
 
 
		});
		down.addMouseListener(new MouseAdapter() {
 
			@Override
			public void mouseReleased(MouseEvent e) {
				downRelease();
//				System.out.println("DOWN-RELEASE\n");
			}
 
 
 
			@Override
			public void mousePressed(MouseEvent e) {
				downPress();
//				System.out.println("DOWN-PRESS\n");
			}
 
 
		});
		left.addMouseListener(new MouseAdapter() {
 
			@Override
			public void mouseReleased(MouseEvent e) {
				leftRelease();
//				System.out.println("LEFT-RELEASE\n");pw.flush();
			}
 
 
			@Override
			public void mousePressed(MouseEvent e) {
				leftPress();
//				System.out.println("LEFT-PRESS\n");pw.flush();
			}
 
 
		});
 
 
		pack();
	}
 
	private void downPress() {
		sendCommand("DOWN-PRESS");
	}
 
	private void sendCommand(String command) {
		try {
			pw.write(command+"\n");pw.flush();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
	}
 
	private void downRelease() {
		sendCommand("DOWN-RELEASE");
	}
 
	private void leftPress() {
		sendCommand("LEFT-PRESS");
	}
 
	private void upRelease() {
		sendCommand("UP-RELEASE");
	}
 
	private void leftRelease() {
		sendCommand("LEFT-RELEASE");
	}
	private void rightRelease() {
		sendCommand("RIGHT-RELEASE");
	}
 
	private void upPress() {
		sendCommand("UP-PRESS");
	}
 
	private void rightPress() {
		sendCommand("RIGHT-PRESS");
	}
	
	private void camRightPress() {
		sendCommand("CAMERA-RIGHT-PRESS");
	}
	
	private void camRightRelease() {
		sendCommand("CAMERA-RIGHT-RELEASE");
	}
	
	private void camLeftPress() {
		sendCommand("CAMERA-LEFT-PRESS");
	}
	
	private void camLeftRelease() {
		sendCommand("CAMERA-LEFT-RELEASE");
	}
}
