package tests.examples;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;

import lejos.hardware.BrickFinder;
import lejos.hardware.Button;
import lejos.hardware.Key;
import lejos.hardware.KeyListener;
import lejos.hardware.ev3.EV3;
import lejos.hardware.lcd.LCD;
import lejos.hardware.motor.EV3LargeRegulatedMotor;
import lejos.hardware.motor.EV3MediumRegulatedMotor;
import lejos.hardware.port.MotorPort;
import lejos.utility.Delay;

/**
 * This was written on jCrete 2015 on a LeJOS session by Steven Chin.
 * 
 * We were given the task to build a lego and do something cool with it.
 * We built a pretty large tank and remote-controlled it via wi-fi with
 * simple socket commands.
 * 
 * For more info, go to https://mihail.stoynov.com/?p=2909
 * 
 * This is a mock server for testing if you don't have a lego at hand.
 * 
 * @author Nayden Gochev, jug.bg
 * @author Mihail Stoynov, jug.bg
 *
 */
public class E3vRemoteControlCarApp {

	static boolean isRunning = true;

	static int _speed = 1000;
	
	static int _cameraSpeed = 50;
	
	public static void main(String[] args) throws IOException {

		
		try (
				final EV3LargeRegulatedMotor leftMottor = new EV3LargeRegulatedMotor(MotorPort.D);
				final EV3LargeRegulatedMotor rightMottor = new EV3LargeRegulatedMotor(MotorPort.A);
				final EV3MediumRegulatedMotor cameraMotor = new EV3MediumRegulatedMotor(MotorPort.C);
				ServerSocket serv = new ServerSocket(19231);
				) {
			LCD.drawString("Awaiting connection...",1,1);
			Socket socket = serv.accept();
			BufferedReader reader = new BufferedReader(new InputStreamReader(
					socket.getInputStream()));
			//Once connected start streaming service
			Ev3VideoStreamTestApp camera = new Ev3VideoStreamTestApp((EV3) BrickFinder.getLocal());
			Thread cameraThread = new Thread(camera);
			cameraThread.start();
			
			LCD.drawString("Connection acquired",1, 2);
			Button.ESCAPE.addKeyListener(new KeyListener() {

				@Override
				public void keyReleased(Key k) {
				}

				@Override
				public void keyPressed(Key k) {
					E3vRemoteControlCarApp.isRunning = false;
				}
			});


			// we copied it from somewhere, not necessary
			Delay.msDelay(3000);


			// the listener with the while readline
			String line;
			while ((line = reader.readLine()) != "STOP" && isRunning) {
				switch (line) {
				case "UP-PRESS":
					leftMottor.setSpeed(_speed);
					// leftMottor.setAcceleration(150);
					leftMottor.backward();
					rightMottor.setSpeed(_speed);
					// rightMottor.setAcceleration(150);
					rightMottor.backward();
					break;
				case "UP-RELEASE":
					leftMottor.stop(true);
					rightMottor.stop(true);
					break;
				case "DOWN-PRESS":
					leftMottor.setSpeed(_speed);
					// leftMottor.setAcceleration(150);
					leftMottor.forward();
					rightMottor.setSpeed(_speed);
					// rightMottor.setAcceleration(150);
					rightMottor.forward();
					break;
				case "DOWN-RELEASE":
					leftMottor.stop(true);
					rightMottor.stop(true);
					break;
				case "LEFT-PRESS":
					rightMottor.setSpeed(_speed);
					// rightMottor.setAcceleration(150);
					rightMottor.backward();
					leftMottor.setSpeed(_speed);
					// leftMottor.setAcceleration(150);
					leftMottor.forward();
					break;
				case "LEFT-RELEASE":
					rightMottor.stop(true);
					leftMottor.stop(true);
					break;
				case "RIGHT-PRESS":
					rightMottor.setSpeed(_speed);
					// rightMottor.setAcceleration(150);
					rightMottor.forward();
					leftMottor.setSpeed(_speed);
					// leftMottor.setAcceleration(150);
					leftMottor.backward();
					break;
				case "RIGHT-RELEASE":
					rightMottor.stop(true);
					leftMottor.stop(true);
					break;
				case "CAMERA-RIGHT-PRESS":
					cameraMotor.setSpeed(_cameraSpeed);
					cameraMotor.backward();					
					break;
				case "CAMERA-RIGHT-RELEASE":
					cameraMotor.stop(true);
					break;
				case "CAMERA-LEFT-PRESS":
					cameraMotor.setSpeed(_cameraSpeed);
					cameraMotor.forward();					
					break;
				case "CAMERA-LEFT-RELEASE":
					cameraMotor.stop(true);
					break;
				case "STOP":
					E3vRemoteControlCarApp.isRunning = false;
					break;
				}
			}
		}
	}
}