package tests.examples;

import java.io.BufferedOutputStream;
import java.io.IOException;
import java.net.Socket;
 
import lejos.hardware.BrickFinder;
import lejos.hardware.Button;
import lejos.hardware.ev3.EV3;
import lejos.hardware.lcd.LCD;
import lejos.hardware.video.Video;
 
public class Ev3VideoStreamTestApp implements Runnable{
    private static final int WIDTH = 160;
    private static final int HEIGHT = 120;
    private static final String HOST = "192.168.1.10";
    private static final int PORT = 55555;
    static EV3 ev3;
    static boolean STOP = false;
 
    Ev3VideoStreamTestApp(EV3 brick) {
    	this.ev3 = brick;
    }

    public synchronized void stopService() {
    	this.STOP = true;
    }
    
	@Override
	public void run() {
		 System.out.println("Starting video thread.");
		 ev3 = (EV3) BrickFinder.getLocal();
	        Video video = ev3.getVideo();
	        try {
				video.open(WIDTH, HEIGHT);
			} catch (IOException e1) {
				System.out.println("Video crapped: "+e1.getMessage());
			}
	        byte[] frame = video.createFrame();
	 
	        Socket sock;
	        BufferedOutputStream bos;
			try {
				sock = new Socket(HOST, PORT);
				bos  = new BufferedOutputStream(sock.getOutputStream());
			} catch (IOException e1) {
				System.out.println("Error: "+e1.getMessage());
				return;
			}
	        
	        long start = System.currentTimeMillis();
	        int frames = 0;
	        LCD.drawString("fps:", 0, 4);
	 
	        while(!STOP) {
	            try {
	                video.grabFrame(frame);
	                int fps = (int) (++frames * 1000f/(System.currentTimeMillis() - start));
	                LCD.drawInt(fps, 5, 4);
	                bos.write(frame);
	                //bos.flush();
	            } catch (IOException e) {
	                break;
	            }
	        }
	        try {
	        	bos.close();
		        sock.close();
		        video.close();
	        }catch(IOException io) {
	        	System.out.println("Error closing streams: "+io.getMessage());
	        }
	        
		
	}
}
